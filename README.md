ParaLabel
=========

A scalable 3D annotation platform that supports KITTI format.

#TODO Badges

#TODO gif

Requirements
============

Ubuntu 20.04.6 LTS (focal) or newer.
Windows is currently not supported but will be in the future.

Installation
============

There are two ways to install the plugin:
* (for users) By downloading the latest ParaLabel binary with associated ParaView version.
* (for developpers) By building the plugin and associated ParaView yourself.

For users
---------

Go to the [releases page] and download the latest version of the paralabel plugin.

In the releases note, check for the compatible ParaView version and download it from the [ParaView download page].


For developpers
---------------

You will first need to [build PDAL] version `2.6.0`, make sure to install `gdal` version `3.4` before.
```sh
sudo add-apt-repository ppa:ubuntugis/ubuntugis-unstable
sudo apt update
sudo apt install libgdal-dev
```

OpenSSL is also needed for testing.
 ```sh
 sudo apt install libssl-dev
 ```

Clone the latest ParaView `master` branch, then [build ParaView] yourself with `python3`, `mpi` and `pdal`:
```sh
mkdir build && cd build
cmake -DPARAVIEW_USE_PYTHON=ON -DPython3_EXECUTABLE=/usr/bin/python3 -DPARAVIEW_USE_MPI=ON -DVTK_SMP_IMPLEMENTATION_TYPE=TBB -DPARAVIEW_ENABLE_PDAL=ON -DCMAKE_BUILD_TYPE=Release ..
```

Now you can clone and build the plugin against paraview:
```sh
git clone git@gitlab.kitware.com:keu-computervision/paralabel/paralabel.git
cd paralabel
mkdir build && cd build
cmake -DParaView_DIR=/PATH/TO/paraview/build -DCMAKE_BUILD_TYPE=Release ..
cmake --build .
```

The plugin should be available in `build/lib/paraview-x.xx/plugins/ParaLabel/ParaLabel.so`

[releases page]: https://gitlab.kitware.com/keu-computervision/paralabel/paralabel/-/releases
[ParaView download page]: https://www.paraview.org/download/
[build PDAL]: https://pdal.io/en/2.6.0/development/compilation/unix.html#unix-compilation
[build ParaView]: https://gitlab.kitware.com/paraview/paraview/-/blob/master/Documentation/dev/build.md#build

Usage
=====

To add/use a plugin to ParaView, follow the simple steps below:
* Open `Tools` > `Manage Plugins...`
* Select `Load New` and open the plugin.

Testing
=======

Tests are avalaible and automatically managed by cmake utilities:
1. Build `paraview` with `-DVTK_DEBUG_LEAKS=ON` and the plugin with `-DPARALABEL_BUILD_TESTING=ON`.
2. Go into the build directory.
3. Run the cmake tests using:
	```bash
	ctest
	```

Tips
====

## Improve render performance

If you have multiple GPUs on your device, it is possible to select which one ParaView will use for rendering. For example if you have an iGPU (with your CPU) and a dGPU, by default the iGPU will be used. It is highly recommended to use the dGPU in that case, especially if you render big 3D data locally.

### On Linux

Launch paraview with the following env variables:
```
__NV_PRIME_RENDER_OFFLOAD=1 __GLX_VENDOR_LIBRARY_NAME=nvidia paraview
```
You can also use `prime-run`.

 Check [this thread](https://discourse.paraview.org/t/select-which-gpu-to-use-when-starting-paraview/3605/12) for more information.

### On Windows

You will need to select the device in the NVIDIA programm settings. For AMD, the process should be similar.

Check [this thread](https://discourse.paraview.org/t/running-on-gpu-from-windows/9339) for more information.

Roadmap
=======

* AI assistance (SAM2 plugin)
* Large-scale segmentation (MRI fibre tract segmentation)
* Windows release

Contributing
============

See [CONTRIBUTING.md][] for instructions to contribute.

Authors and acknowledgment
==========================
Team:
* [Loic Tetrel](loic.tetrel@kitware.com) - *Lead Data Scientist*: **Project Lead**
* [Kitware SAS](kitware@kitware.com) - *R&D engineer*: **QA**

Acknowledgments:
* [Timothee Teyssier](timothee.teyssier@cpe.fr) - *Intern*: **Main Developper**
* [Ihab El Amrani](ihab.elamrani@kitware.com) - *Intern*: **Developper**

License
=======

ParaLabel is distributed under the OSI-approved Apache 2.0 [License][].
See [Copyright][] for details.

[Copyright]: Copyright.txt
[License]: LICENSE

Project status
==============

In active development.
