Make sure to build `paraview` with `Debug`.

[Plugin Build]: README.md#Installation

Release
=======

To create a release:
1. Make sure the main branch latest commit include your changes
2. Update the [version.txt](version.txt) file with the version number
2. Create a tag with version number that target the `main` branch
3. After building the plugin, install it
	```bash
	cd build 
	cmake -DCMAKE_INSTALL_PREFIX="install" ..
	cmake --install .
	```
4. You can create an archive with name "ParaLabelvX.X.X", upload it and create a release on gitlab
