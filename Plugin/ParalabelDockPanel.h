// SPDX-FileCopyrightText: Copyright (c) Kitware Inc.
// SPDX-FileCopyrightText: Copyright (c) Sandia Corporation
// SPDX-License-Identifier: BSD-3-Clause
/**
 * @class ParalabelDockPanel
 * @brief This class is used to control all the buttons which are used for Paralabel they are stored
 * in a dock panel
 */

#ifndef ParalabelDockPanel_h
#define ParalabelDockPanel_h

#include <QDockWidget>

#include <pqDataRepresentation.h>
#include <pqPipelineSource.h>
#include <vtkBoxWidgetOriented.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkVector.h>

class ParalabelDockPanel : public QDockWidget
{
  Q_OBJECT
  typedef QDockWidget Superclass;

public:
  ParalabelDockPanel(const QString& t, QWidget* p = 0, Qt::WindowFlags f = 0)
    : Superclass(t, p, f)
  {
    this->constructor();
  }
  ParalabelDockPanel(QWidget* p = 0, Qt::WindowFlags f = 0)
    : Superclass(p, f)
  {
    this->constructor();
  }
  ~ParalabelDockPanel();

  /**
   * @brief updateBoxWidgets
   * Update the box widgets based on the current representation when the boxWidget is modified
   */
  void updateBoxWidgets();

  /**
   * @brief removeCategoryFromRepresentation
   * Update the QComboBox with only the labels of the active representation
   */
  void removeCategoryFromRepresentation(pqRepresentation* rep);

  /**
   * @brief enableBoxWidgets
   * Enable or disable the box widgets
   */
  void enableBoxWidgets(bool enable);

  void GetWidgetDecomposedTransform(
    vtkVector3d& outTranslation, vtkVector3d& outRotation, vtkVector3d& outLength);

protected:
  class pqInternal;
  pqInternal* Internal = nullptr;

public Q_SLOTS:

public slots:
  void addLabel();
  void addCategory();
  void removeLabel();
  void selectAnnotation();
  /**
   * @brief ParalabelDockPanel::onProxyAdded
   * Update the category combo box when a proxy to a PDC is created (after source creation)
  */
  void onProxyAdded();
  void onRepresentationAdded();
  void onDataUpdated(pqPipelineSource* source);
  void onAddAnnotation();
  void onRemoveAnnotation();
  void onEditAnnotation();
  void onViewAdded(pqView* proxy);
  void resetViews();


private:
  enum ViewIndex
  {
    MAIN = 0,
    TOP,
    SIDE,
    FRONT,
    VIEW_COUNT
  };

  bool annotationAdded = false;
  unsigned int modifyInteractorStyleFlag = 0;
  int idxToModify = -1;
  std::string activeSelectedBlockName;
  double length;
  double scales[3];
  double camPos[3];

  std::array<vtkSmartPointer<vtkBoxWidgetOriented>, ViewIndex::VIEW_COUNT> widgets;

  void constructor();

  /**
   * @brief Get the selected annotation. Return its index, or -1 if selection failed in any way
   */
  bool getSelectedAnnotation(vtkPolyData*& outPolyData, int& annotationIdx);
};

#endif
