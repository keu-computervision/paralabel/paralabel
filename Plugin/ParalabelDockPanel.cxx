#include "ParalabelDockPanel.h"
#include "CenteredComboBox.h"
#include "CenteredComboBoxDelegate.h"
#include "ui_ParalabelDockPanel.h"

#include <QShortcut>

#include <iostream>

#include <pqActiveObjects.h>
#include <pqApplicationCore.h>
#include <pqDataRepresentation.h>
#include <pqObjectBuilder.h>
#include <pqOutputPort.h>
#include <pqPVApplicationCore.h>
#include <pqPipelineSource.h>
#include <pqProxySelection.h>
#include <pqProxyWidget.h>
#include <pqRenderView.h>
#include <pqScalarBarRepresentation.h>
#include <pqSelectionManager.h>
#include <pqServer.h>
#include <pqServerManagerModel.h>

#include "vtkSMSelectionHelper.h"
#include <qobject.h>
#include <vtkPVArrayInformation.h>
#include <vtkPVDataInformation.h>
#include <vtkPVDataSetAttributesInformation.h>
#include <vtkSMColorMapEditorHelper.h>
#include <vtkSMOutputPort.h>
#include <vtkSMPropertyHelper.h>
#include <vtkSMRenderViewProxy.h>
#include <vtkSMRepresentationProxy.h>
#include <vtkSMSourceProxy.h>
#include <vtkSMStringVectorProperty.h>

#include "vtkCellPicker.h"
#include "vtkCompositeRepresentation.h"
#include "vtkFieldData.h"
#include "vtkInteractorStyle.h"
#include "vtkPartitionedDataSet.h"
#include "vtkPolyData.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"
#include "vtkRendererCollection.h"
#include "vtkSelectionNode.h"
#include "vtkSelectionSource.h"
#include "vtkStringArray.h"
#include <vtkBoxWidgetOriented.h>
#include <vtkCamera.h>
#include <vtkCubeSource.h>
#include <vtkDataAssembly.h>
#include <vtkDataObject.h>
#include <vtkPartitionedDataSetCollection.h>
#include <vtkSmartPointer.h>
#include <vtkTransform.h>

class ProcessEventsCallback : public vtkCommand
{
public:
  static ProcessEventsCallback* New() { return new ProcessEventsCallback; }

  void SetDockPanel(ParalabelDockPanel* panel) { this->DockPanel = panel; }

  virtual void Execute(vtkObject* caller, unsigned long eventId, void* callData) override
  {
    if (eventId == vtkBoxWidgetOriented::WidgetModifiedEvent)
    {
      if (this->DockPanel)
      {
        this->DockPanel->updateBoxWidgets();

        // Update currently active filter's properties
        pqPipelineSource* activeSource = pqActiveObjects::instance().activeSource();
        if (auto sourceProxy = vtkSMSourceProxy::SafeDownCast(activeSource->getProxy()))
        {
          auto positionProperty = sourceProxy->GetProperty("Position");
          auto rotationProperty = sourceProxy->GetProperty("Rotation");
          auto lenghtProperty = sourceProxy->GetProperty("Length");

          if (positionProperty && rotationProperty && lenghtProperty)
          {
            vtkVector3d position, rotation, length;
            this->DockPanel->GetWidgetDecomposedTransform(position, rotation, length);

            vtkSMPropertyHelper(positionProperty).Set(position.GetData(), 3);
            vtkSMPropertyHelper(rotationProperty).Set(rotation.GetData(), 3);
            vtkSMPropertyHelper(lenghtProperty).Set(length.GetData(), 3);
          }
        }
      }
    }
    if (eventId == vtkCommand::vtkCommand::LeftButtonDoubleClickEvent)
    {
      if (this->DockPanel)
      {
        this->DockPanel->selectAnnotation();
      }
    }
  }

private:
  ParalabelDockPanel* DockPanel;
};

class ParalabelDockPanel::pqInternal
{
public:
  Ui::ParalabelDockPanel ui;
};

//-------------------------------------------------------------------------
void ParalabelDockPanel::GetWidgetDecomposedTransform(
  vtkVector3d& outTranslation, vtkVector3d& outRotation, vtkVector3d& outLength)
{
  vtkNew<vtkTransform> transform;
  this->widgets[ViewIndex::MAIN]->GetTransform(transform);

  transform->GetPosition(outTranslation.GetData());
  transform->GetOrientation(outRotation.GetData());
  transform->GetScale(outLength.GetData());
}

//-------------------------------------------------------------------------
void ParalabelDockPanel::constructor()
{
  this->setWindowTitle("Paralabel Dock Panel");
  QWidget* t_widget = new QWidget(this);
  this->Internal = new ParalabelDockPanel::pqInternal();
  Ui::ParalabelDockPanel& ui = this->Internal->ui;
  ui.setupUi(t_widget);
  this->setWidget(t_widget);

  CenteredComboBox* comboBoxCategories = new CenteredComboBox(this);
  ui.verticalLayout->replaceWidget(ui.comboBoxCategories, comboBoxCategories);
  delete ui.comboBoxCategories;
  ui.comboBoxCategories = comboBoxCategories;

  CenteredComboBoxDelegate* centeredComboBoxDelegate = new CenteredComboBoxDelegate(this);
  ui.comboBoxCategories->setItemDelegate(centeredComboBoxDelegate);

  // create shortcuts
  ui.comboBoxCategories->addItem("Categories");

  // connect the ui buttons to slots
  connect(ui.addCategory, SIGNAL(clicked()), this, SLOT(addLabel()));
  connect(ui.editLabel, SIGNAL(returnPressed()), this, SLOT(addLabel()));
  connect(ui.removeCategory, SIGNAL(clicked()), this, SLOT(removeLabel()));
  connect(ui.comboBoxCategories, SIGNAL(currentIndexChanged(int)), this, SLOT(addCategory()));
  connect(ui.removeAnnotation, &QPushButton::clicked, this, &ParalabelDockPanel::onRemoveAnnotation);
  connect(ui.resetUI, SIGNAL(clicked()), this, SLOT(resetViews()));

  connect(ui.editAnnotation, &QPushButton::clicked, this, &ParalabelDockPanel::onEditAnnotation);
  connect(ui.addAnnotation, &QPushButton::clicked, this, &ParalabelDockPanel::onAddAnnotation);

  // connect the server manager model to the slots
  pqApplicationCore* app = pqApplicationCore::instance();
  pqServerManagerModel* smModel = app->getServerManagerModel();
  pqActiveObjects* activeObjects = &pqActiveObjects::instance();
  QObject::connect(smModel, &pqServerManagerModel::proxyAdded, this,
    &ParalabelDockPanel::onProxyAdded);
  QObject::connect(smModel, &pqServerManagerModel::representationAdded, this,
    &ParalabelDockPanel::onRepresentationAdded);
  QObject::connect(smModel, &pqServerManagerModel::representationRemoved, this,
    &ParalabelDockPanel::removeCategoryFromRepresentation);
  QObject::connect(smModel, &pqServerManagerModel::viewAdded, this,
    &ParalabelDockPanel::onViewAdded);
  QObject::connect(
    smModel, &pqServerManagerModel::dataUpdated, this, &ParalabelDockPanel::onDataUpdated);

  vtkNew<ProcessEventsCallback> callback;
  callback->SetDockPanel(this);

  for (auto i = 0; i < ViewIndex::VIEW_COUNT; ++i)
  {
    this->widgets[i].TakeReference(vtkBoxWidgetOriented::New());
    this->widgets[i]->AddObserver(vtkBoxWidgetOriented::WidgetModifiedEvent, callback);
  }

  auto views = pqPVApplicationCore::instance()->getServerManagerModel()->findItems<pqView*>(
    activeObjects->activeServer());

  pqRenderView* mainRenderView = qobject_cast<pqRenderView*>(views[0]);
  auto interactor = mainRenderView->getRenderViewProxy()->GetInteractor();
  vtkInteractorStyle* style = vtkInteractorStyle::SafeDownCast(interactor->GetInteractorStyle());
  style->AddObserver(vtkCommand::LeftButtonDoubleClickEvent, callback);

  // Needs to set widget for default render view as well when starting paraview
  this->onViewAdded(views[0]);
}

//-------------------------------------------------------------------------
ParalabelDockPanel::~ParalabelDockPanel()
{
  delete this->Internal;
}

//-------------------------------------------------------------------------
void ParalabelDockPanel::onDataUpdated(pqPipelineSource* source)
{
  this->enableBoxWidgets(false);
}

//-------------------------------------------------------------------------
void ParalabelDockPanel::onAddAnnotation()
{

  pqObjectBuilder* builder = pqApplicationCore::instance()->getObjectBuilder();
  pqPipelineSource* activeSource = pqActiveObjects::instance().activeSource();

  vtkVector3d focalPoint;
  pqRenderView* renderView = qobject_cast<pqRenderView*>(pqActiveObjects::instance().activeView());
  renderView->getRenderViewProxy()->GetActiveCamera()->GetFocalPoint(focalPoint.GetData());

  QString label = this->Internal->ui.comboBoxCategories->currentText();

  if (label == "Categories" || label == "")
  {
    std::cerr << "No label selected" << std::endl;
    return;
  }

  pqPipelineSource* addAnnotationFilter =
    builder->createFilter("filters", "AddAnnotation", activeSource);
  vtkSMSourceProxy* addAnnotationFilterProxy =
    vtkSMSourceProxy::SafeDownCast(addAnnotationFilter->getProxy());

  vtkVector3d defaultSize = {30, 30, 30};
  vtkSMPropertyHelper(addAnnotationFilterProxy, "Label").Set(label.toStdString().c_str());
  vtkSMPropertyHelper(addAnnotationFilterProxy, "Position").Set(focalPoint.GetData(), 3);
  vtkSMPropertyHelper(addAnnotationFilterProxy, "Length").Set(defaultSize.GetData(), 3);

  // TODO: default scale and orientation based on view
  addAnnotationFilterProxy->UpdateVTKObjects();

  // editAnnotationFilterProxy->UpdatePipeline();
  // editAnnotationFilterProxy->UpdatePropertyInformation();


  // Place the widget and forward to other views
  vtkNew<vtkTransform> transform;
  transform->Identity();
  transform->Translate(focalPoint.GetData());
  transform->Scale(defaultSize.GetData());

  this->widgets[ViewIndex::MAIN]->SetTransform(transform);
  this->updateBoxWidgets();
  this->enableBoxWidgets(true);
}

bool ParalabelDockPanel::getSelectedAnnotation(vtkPolyData*& outPolyData, int& outIndex)
{
  pqSelectionManager* selectionManager = pqPVApplicationCore::instance()->selectionManager();
  if (!selectionManager->hasActiveSelection())
  {
    std::cerr << "No selection found" << std::endl;
    return false;
  }

  auto selectedPort = selectionManager->getSelectedPort();
  if (!selectedPort)
  {
    std::cerr << "No selected ports found" << std::endl;
    return false;
  }

  vtkSMSourceProxy* selectedSourceProxy = selectedPort->getSelectionInput();
  if (!selectedSourceProxy)
  {
    std::cerr << "No selected source proxy found" << std::endl;
    return false;
  }

  pqObjectBuilder* builder = pqApplicationCore::instance()->getObjectBuilder();
  pqServer* server = pqActiveObjects::instance().activeServer();
  pqPipelineSource* activeSource = pqActiveObjects::instance().activeSource();
  vtkSMSourceProxy* activeSourceProxy = activeSource->getSourceProxy();

  auto getSelection = builder->createProxy("filters", "GetSelection", server, "");
  vtkSMPropertyHelper(getSelection, "SelectionInput").Set(selectedSourceProxy);
  vtkSMPropertyHelper(getSelection, "SelectionSource").Set(activeSourceProxy);
  getSelection->UpdateVTKObjects();

  auto getSelectionOutput = vtkSMSourceProxy::SafeDownCast(getSelection);
  if (!getSelectionOutput)
  {
    std::cerr << "No getSelectionOutput found" << std::endl;
    return false;
  }
  
  getSelectionOutput->UpdatePipeline();
  vtkPVDataInformation* dataInfo = getSelectionOutput->GetDataInformation();
  if (!dataInfo)
  {
    std::cerr << "No dataInfo found" << std::endl;
    return false;
  }

  vtkPVDataSetAttributesInformation* attributesInfo =
    dataInfo->GetAttributeInformation(vtkDataObject::FIELD);
  if (!attributesInfo)
  {
    std::cerr << "No attributesInfo found" << std::endl;
    return false;
  }

  vtkPVArrayInformation* array = attributesInfo->GetArrayInformation("Selection");
  if (!array)
  {
    std::cerr << "No array found" << std::endl;
    return false;
  }

  std::string pathName = array->GetStringValue(0);
  std::string blockName = pathName.substr(pathName.find_last_of("/") + 1);
  std::string blockNumber = blockName.substr(5);
  int idBlock = std::stoi(blockNumber);

  // now we need to get the proxy of the polydata which is in the block idBlock
  pqDataRepresentation* activeRepresentation = pqActiveObjects::instance().activeRepresentation();
  vtkSMProxy* activeRepProxy = activeRepresentation->getProxy();
  vtkObjectBase* activeRep = activeRepProxy->GetClientSideObject();
  vtkCompositeRepresentation* activeCompositeRep =
    vtkCompositeRepresentation::SafeDownCast(activeRep);
  vtkDataObject* activeDataObject = activeCompositeRep->GetRenderedDataObject(0);
  if (!activeDataObject)
  {
    std::cerr << "No active data object found" << std::endl;
    return false;
  }

  vtkPartitionedDataSetCollection* pdc =
    vtkPartitionedDataSetCollection::SafeDownCast(activeDataObject);
  if (!pdc)
  {
    std::cerr << "No pdc found" << std::endl;
    return false;
  }

  vtkDataAssembly* dataAssembly = pdc->GetDataAssembly();
  if (!dataAssembly)
  {
    std::cerr << "No dataAssembly found" << std::endl;
    return false;
  }

  auto partitionIndices = dataAssembly->GetDataSetIndices(idBlock);
  if (partitionIndices.size() == 0)
  {
    std::cerr << "No partitionIdx found" << std::endl;
    return false;
  }

  outIndex = partitionIndices[0];
  vtkPartitionedDataSet* partitionedDataSet = pdc->GetPartitionedDataSet(outIndex);
  outPolyData = vtkPolyData::SafeDownCast(partitionedDataSet->GetPartitionAsDataObject(0));
  if (!outPolyData)
  {
    std::cerr << "No polyData found" << std::endl;
    return false;
  }

  return true;
}

//-------------------------------------------------------------------------
void ParalabelDockPanel::onEditAnnotation()
{
  vtkPolyData* selectedPolyData = nullptr;
  int selectedAnnotationIdx;
  if (!this->getSelectedAnnotation(selectedPolyData, selectedAnnotationIdx))
  {
    return;
  }

  // Get current annotation infos
  vtkFieldData* fieldData = selectedPolyData->GetFieldData();

  vtkStringArray* labelArray = vtkStringArray::SafeDownCast(fieldData->GetAbstractArray("label"));
  std::string labelValue = labelArray->GetValue(0);

  vtkVector3d position;
  vtkDataArray* positionArray = fieldData->GetArray("position");
  positionArray->GetTuple(0, position.GetData());

  vtkVector3d scale;
  vtkDataArray* scaleArray = fieldData->GetArray("scale");
  scaleArray->GetTuple(0, scale.GetData());

  vtkVector3d rotation;
  vtkDataArray* orientationArray = fieldData->GetArray("rotationXYZ");
  orientationArray->GetTuple(0, rotation.GetData());

  // Create the filter and set its properties
  pqObjectBuilder* builder = pqApplicationCore::instance()->getObjectBuilder();

  auto editAnnotationFilter = builder->createFilter(
    "filters", "EditAnnotation", pqActiveObjects::instance().activeSource());

  vtkSMSourceProxy* editAnnotationFilterProxy =
    vtkSMSourceProxy::SafeDownCast(editAnnotationFilter->getProxy());
  vtkSMPropertyHelper(editAnnotationFilterProxy, "SelectedAnnotationIdx")
    .Set(selectedAnnotationIdx);
  vtkSMPropertyHelper(editAnnotationFilterProxy, "Label").Set(labelValue.c_str());
  vtkSMPropertyHelper(editAnnotationFilterProxy, "Position").Set(position.GetData(), 3);
  vtkSMPropertyHelper(editAnnotationFilterProxy, "Rotation").Set(rotation.GetData(), 3);
  vtkSMPropertyHelper(editAnnotationFilterProxy, "Length").Set(scale.GetData(), 3);

  editAnnotationFilterProxy->UpdateVTKObjects();
  editAnnotationFilterProxy->UpdatePropertyInformation();

  // Place the widget and forward to other views
  vtkNew<vtkTransform> transform;
  transform->Identity();
  transform->Translate(position.GetData());
  transform->RotateZ(rotation[2]);
  transform->RotateX(rotation[0]);
  transform->RotateY(rotation[1]);
  transform->Scale(scale.GetData());

  this->widgets[ViewIndex::MAIN]->SetTransform(transform);
  this->updateBoxWidgets();
  this->enableBoxWidgets(true);
}

//-------------------------------------------------------------------------
void ParalabelDockPanel::resetViews()
{
  auto views = pqPVApplicationCore::instance()->getServerManagerModel()->findItems<pqView*>(
    pqActiveObjects::instance().activeServer());
  for (auto& view : views)
  {
    pqRenderView* renderView = qobject_cast<pqRenderView*>(view);
    QString viewName = view->getSMName();
    if (viewName == "Main")
    {
      pqActiveObjects::instance().setActiveView(view);
    }
    else if (viewName == "Side")
    {
      renderView->resetViewDirectionToPositiveX();
    }
    else if (viewName == "Front")
    {
      renderView->resetViewDirectionToPositiveY();
    }
    else if (viewName == "Top")
    {
      renderView->resetViewDirectionToNegativeZ();
    }
  }

  pqDataRepresentation* activeRepresentation = pqActiveObjects::instance().activeRepresentation();
  if (!activeRepresentation)
  {
    return;
  }

  pqPipelineSource* activeSource = activeRepresentation->getInput();
  if (activeSource->getSourceProxy()->GetDataInformation()->GetArrayInformation(
        "label", vtkDataObject::FIELD)) // check if the activeSource has a label field data
  {
    vtkSMColorMapEditorHelper::SetScalarColoring(
      activeRepresentation->getProxy(), "label", vtkDataObject::FIELD);
  }

  auto rep =
    pqPVApplicationCore::instance()->getServerManagerModel()->findItems<pqDataRepresentation*>(
      pqActiveObjects::instance().activeServer());
  for (int i = 0; i < rep.size(); i++)
  {
    if (rep[i]->getInput()->getSourceProxy()->GetDataInformation()->GetArrayInformation(
          "label", vtkDataObject::FIELD)) // check if the activeSource has a label field data
    {
      vtkSMColorMapEditorHelper::SetScalarColoring(
        rep[i]->getProxy(), "label", vtkDataObject::FIELD);
    }
  }

  auto scalarBars =
    pqPVApplicationCore::instance()->getServerManagerModel()->findItems<pqScalarBarRepresentation*>(
      pqActiveObjects::instance().activeServer());
  pqObjectBuilder* builder = pqApplicationCore::instance()->getObjectBuilder();
  for (auto& scalarBar : scalarBars)
  {
    pqView* viewBar = scalarBar->getView();
    QString viewBarName = viewBar->getSMName();
    if (viewBarName != "Main")
    {
      builder->destroy(scalarBar);
    }
  }
}

//-------------------------------------------------------------------------
void ParalabelDockPanel::updateBoxWidgets()
{
  pqView* activeView = pqActiveObjects::instance().activeView();
  auto views = pqPVApplicationCore::instance()->getServerManagerModel()->findItems<pqView*>(
    pqActiveObjects::instance().activeServer());

  // Get active view index
  int activeViewIdx = -1;
  for (int viewIdx = 0; viewIdx < views.size(); ++viewIdx)
  {
    if (views[viewIdx] == activeView)
    {
      activeViewIdx = viewIdx;
      break;
    }
  }

  vtkNew<vtkPolyData> pd;
  this->widgets[activeViewIdx]->GetPolyData(pd);
  vtkVector3d center;
  pd->GetCenter(center.GetData());

  vtkNew<vtkTransform> transform;
  this->widgets[activeViewIdx]->GetTransform(transform);

  for (int viewIdx = 0; viewIdx < ViewIndex::VIEW_COUNT; ++viewIdx)
  {
    if (viewIdx == activeViewIdx)
    {
      continue;
    }

    // Update widget placement
    this->widgets[viewIdx]->SetTransform(transform);

    // Move camera to center on the widget
    pqRenderView* renderView = qobject_cast<pqRenderView*>(views[viewIdx]);
    vtkCamera* camera = renderView->getRenderViewProxy()->GetActiveCamera();

    vtkVector3d oldPosition;
    camera->GetPosition(oldPosition.GetData());

    switch (viewIdx)
    {
      case ViewIndex::TOP:
        camera->SetPosition(oldPosition[0], center[1], center[2]);
        break;
      case ViewIndex::SIDE:
        camera->SetPosition(center[0], oldPosition[1], center[2]);
        break;
      case ViewIndex::FRONT:
        camera->SetPosition(center[0], center[1], oldPosition[2]);
        break;
    }

    camera->SetFocalPoint(center.GetData());

    views[viewIdx]->render();
  }
}

//-------------------------------------------------------------------------
void ParalabelDockPanel::selectAnnotation()
{
  pqView* activeView = pqActiveObjects::instance().activeView();
  pqRenderView* activeRenderView = qobject_cast<pqRenderView*>(activeView);
  vtkRenderWindowInteractor* interactor = activeRenderView->getRenderViewProxy()->GetInteractor();

  vtkVector2i posScreen;
  interactor->GetEventPosition(posScreen.GetData());

  vtkNew<vtkCellPicker> picker;
  picker->SetTolerance(0.005);

  vtkRenderer* renderer = interactor->GetRenderWindow()->GetRenderers()->GetFirstRenderer();
  picker->Pick(posScreen[0], posScreen[1], 0, renderer);

  double positionPicker[3];
  picker->GetPickPosition(positionPicker);

  pqDataRepresentation* activeRepresentation = pqActiveObjects::instance().activeRepresentation();
  if (!activeRepresentation)
  {
    std::cerr << "No active representation found" << std::endl;
    return;
  }
  vtkObjectBase* ObjectBase = activeRepresentation->getProxy()->GetClientSideObject();
  vtkCompositeRepresentation* compositeRepresentation =
    vtkCompositeRepresentation::SafeDownCast(ObjectBase);
  vtkDataObject* dataObject = compositeRepresentation->GetRenderedDataObject(0);
  vtkPartitionedDataSetCollection* pdc = vtkPartitionedDataSetCollection::SafeDownCast(dataObject);
  if (!pdc)
  {
    std::cerr << "No pdc found" << std::endl;
    return;
  }

  int closestId = 0;
  double distance[pdc->GetNumberOfPartitionedDataSets()];

  for (unsigned int i = 0; i < pdc->GetNumberOfPartitionedDataSets(); i++)
  {
    vtkPartitionedDataSet* partitionedDataSet = pdc->GetPartitionedDataSet(i);
    vtkDataObject* dataObject =
      partitionedDataSet ? partitionedDataSet->GetPartitionAsDataObject(0) : nullptr;
    vtkPolyData* polyData = dataObject ? vtkPolyData::SafeDownCast(dataObject) : nullptr;

    if (!partitionedDataSet || !dataObject || !polyData)
    {
      std::cerr << "Error: Unable to retrieve necessary data." << std::endl;
      return;
    }
    double center[3];
    polyData->GetCenter(center);
    double distanceToCenter = sqrt(pow(center[0] - positionPicker[0], 2) +
      pow(center[1] - positionPicker[1], 2) + pow(center[2] - positionPicker[2], 2));
    distance[i] = distanceToCenter;
  }
  // check if the min distance is too big then return
  if (*std::min_element(distance, distance + pdc->GetNumberOfPartitionedDataSets()) > 70)
  {
    return;
  }

  closestId =
    std::min_element(distance, distance + pdc->GetNumberOfPartitionedDataSets()) - distance;
  pqPipelineSource* source = pqActiveObjects::instance().activeSource();
  auto producer = source->getOutputPort(0);
  auto proxy = producer->getOutputPortProxy();

  auto sourceProxy = proxy->GetSourceProxy();

  // create a vtkSelectionNode with the closestId
  vtkNew<vtkSelectionSource> selectionSource;
  selectionSource->SetContentType(vtkSelectionNode::BLOCK_SELECTORS);
  selectionSource->SetFieldType(vtkSelectionNode::CELL);
  selectionSource->SetArrayName("Assembly");

  // get the path to the block in the dataassembly
  vtkDataAssembly* dataAssembly = pdc->GetDataAssembly();
  if (!dataAssembly)
  {
    std::cerr << "No data assembly found" << std::endl;
    return;
  }

  // get the value of field data nodeId of the closestId polydata from pdc
  vtkDataObject* dataObjectClosest =
    pdc->GetPartitionedDataSet(closestId)->GetPartitionAsDataObject(0);
  vtkPolyData* polyDataClosest = vtkPolyData::SafeDownCast(dataObjectClosest);
  if (!polyDataClosest)
  {
    std::cerr << "No polydata closest found" << std::endl;
    return;
  }
  vtkFieldData* fieldData = polyDataClosest->GetFieldData();
  vtkDataArray* nodeIdArray = fieldData->GetArray("uid");
  if (!nodeIdArray)
  {
    std::cerr << "No nodeId array found" << std::endl;
    return;
  }
  int nodeIdValue = nodeIdArray->GetTuple1(0);
  std::string blockname = "Block" + std::to_string(nodeIdValue);
  this->activeSelectedBlockName = blockname;
  std::string path = dataAssembly->GetNodePath(nodeIdValue);
  std::string label = path.substr(6, path.find_last_of("/") - 6);
  int index = this->Internal->ui.comboBoxCategories->findText(label.c_str());

  if (index == -1)
  {
    this->Internal->ui.comboBoxCategories->addItem(label.c_str());
    index = this->Internal->ui.comboBoxCategories->count() - 1;
  }

  this->Internal->ui.comboBoxCategories->setCurrentIndex(index);

  selectionSource->AddBlockSelector(path.c_str());
  selectionSource->Update();

  auto selectionSourceProxy = vtk::TakeSmartPointer(
    vtkSMSourceProxy::SafeDownCast(vtkSMSelectionHelper::NewSelectionSourceFromSelection(
      sourceProxy->GetSession(), selectionSource->GetOutput())));

  auto appendSelections = vtk::TakeSmartPointer(vtkSMSourceProxy::SafeDownCast(
    vtkSMSelectionHelper::NewAppendSelectionsFromSelectionSource(selectionSourceProxy)));

  if (appendSelections)
  {
    sourceProxy->SetSelectionInput(proxy->GetPortIndex(), appendSelections, 0);

    auto smmodel = pqApplicationCore::instance()->getServerManagerModel();
    auto selectionModel = pqPVApplicationCore::instance()->selectionManager();
    selectionModel->select(smmodel->findItem<pqOutputPort*>(proxy));
  }
}

//-------------------------------------------------------------------------
void ParalabelDockPanel::onRemoveAnnotation()
{
  pqSelectionManager* selectionManager = pqPVApplicationCore::instance()->selectionManager();
  bool hasSelection = selectionManager->hasActiveSelection();
  if (!hasSelection)
  {
    std::cerr << "No selection found" << std::endl;
    return;
  }
  auto selectedPort = selectionManager->getSelectedPort();
  if (!selectedPort)
  {
    std::cerr << "No selected ports found" << std::endl;
    return;
  }

  vtkSMSourceProxy* selectedSourceProxy = selectionManager->getSelectedPort()->getSelectionInput();
  if (!selectedSourceProxy)
  {
    std::cerr << "No selected source proxy found" << std::endl;
    return;
  }

  pqObjectBuilder* builder = pqApplicationCore::instance()->getObjectBuilder();
  pqServer* server = pqActiveObjects::instance().activeServer();
  pqPipelineSource* activeSource = pqActiveObjects::instance().activeSource();
  if (!activeSource)
  {
    std::cerr << "No active source found" << std::endl;
    return;
  }

  vtkSMSourceProxy* activeSourceProxy = activeSource->getSourceProxy();
  if (!activeSourceProxy)
  {
    std::cerr << "No active source proxy found" << std::endl;
    return;
  }

  auto getSelection = builder->createProxy("filters", "GetSelection", server, "");
  vtkSMPropertyHelper(getSelection, "SelectionInput").Set(selectedSourceProxy);
  vtkSMPropertyHelper(getSelection, "SelectionSource").Set(activeSourceProxy);

  getSelection->UpdateVTKObjects();

  auto getSelectionOutput = vtkSMSourceProxy::SafeDownCast(getSelection);
  if (!getSelectionOutput)
  {
    std::cerr << "No get selection output found" << std::endl;
    return;
  }

  getSelectionOutput->UpdatePipeline();

  vtkPVDataInformation* dataInfo = getSelectionOutput->GetDataInformation();
  if (!dataInfo)
  {
    std::cerr << "No data information found" << std::endl;
    return;
  }

  vtkPVDataSetAttributesInformation* dataAttributes =
    dataInfo->GetAttributeInformation(vtkDataObject::FIELD);
  if (!dataAttributes)
  {
    std::cerr << "No data attributes found" << std::endl;
    return;
  }

  vtkPVArrayInformation* arrayInfo = dataAttributes->GetArrayInformation("Selection");
  if (!arrayInfo)
  {
    std::cerr << "No array information found" << std::endl;
    return;
  }

  auto removeFilter = builder->createFilter("filters", "RemoveAnnotations", activeSource);
  for (int i = 0; i < arrayInfo->GetNumberOfTuples(); i++)
  {
    vtkSMPropertyHelper(removeFilter->getProxy(), "Selection")
      .Set(i, arrayInfo->GetStringValue(i));
  }
  removeFilter->getProxy()->UpdateVTKObjects();
  removeFilter->getProxy()->UpdatePropertyInformation();
}

//-------------------------------------------------------------------------
void ParalabelDockPanel::onRepresentationAdded()
{
  if (this->modifyInteractorStyleFlag == 0)
  {
    auto views = pqPVApplicationCore::instance()->getServerManagerModel()->findItems<pqView*>(
      pqActiveObjects::instance().activeServer());
    vtkNew<ProcessEventsCallback> callback;
    callback->SetDockPanel(this);

    for (int i = 1; i < views.size(); i++)
    {
      pqRenderView* renderView = qobject_cast<pqRenderView*>(views[i]);
      vtkRenderWindowInteractor* interactorIdx = renderView->getRenderViewProxy()->GetInteractor();
      vtkInteractorStyle* styleIdx =
        vtkInteractorStyle::SafeDownCast(interactorIdx->GetInteractorStyle());
      styleIdx->AddObserver(vtkCommand::LeftButtonDoubleClickEvent, callback);
    }

    this->modifyInteractorStyleFlag = 1;
  }
}

//-------------------------------------------------------------------------
void ParalabelDockPanel::onViewAdded(pqView* view)
{
    if (view == nullptr)
    {
        return;
    }
    std::array<double, 6> defaultBounds = { -1, 1, -1, 1, -1, 1 };
    QString viewName = view->getSMName();
    pqRenderView* activeView = qobject_cast<pqRenderView*>(view);
    vtkRenderWindowInteractor* interactor = activeView->getRenderViewProxy()->GetInteractor();
    vtkInteractorStyle* style = vtkInteractorStyle::SafeDownCast(interactor->GetInteractorStyle());
    style->AutoAdjustCameraClippingRangeOff();

    // Get active view index: this implies that the order when adding views is consistent with ViewIndex
    // Identifying the view by its name with getSMName() does not work because this is trigerred before name changes
    auto views = pqPVApplicationCore::instance()->getServerManagerModel()->findItems<pqView*>(
        pqActiveObjects::instance().activeServer());
    int activeViewIdx = -1;
    for (int viewIdx = 0; viewIdx < views.size(); ++viewIdx)
    {
        if (views[viewIdx] == activeView)
        {
        activeViewIdx = viewIdx;
        break;
        }
    }

    this->widgets[activeViewIdx]->SetInteractor(interactor);
    this->widgets[activeViewIdx]->PlaceWidget(defaultBounds.data());
    this->widgets[activeViewIdx]->SetEnabled(false);
}

//-------------------------------------------------------------------------
void ParalabelDockPanel::addCategory()
{
  if (this->Internal->ui.comboBoxCategories->count() == 0)
  {
    this->Internal->ui.comboBoxCategories->addItem("Categories");
  }
}

//-------------------------------------------------------------------------
void ParalabelDockPanel::addLabel()
{
  QString label = this->Internal->ui.editLabel->text();
  if (label.isEmpty())
  {
    std::cerr << "No label entered" << std::endl;
    return;
  }

  if (label.contains("*"))
  {
    std::cerr << "Label cannot contain the character '*'" << std::endl;
    return;
  }

  // if there is a space in the label, replace it with an underscore
  if (label.contains(" "))
  {
    label.replace(" ", "_");
  }

  // if the label is all lowercase, capitalize the first letter for consistency
  label = label.at(0).toUpper() + label.mid(1);
  if (this->Internal->ui.comboBoxCategories->findText(label) != -1)
  {
    std::cerr << "Label already exists" << std::endl;
    return;
  }

  // if comboxLabels contains "Categories", remove it
  this->Internal->ui.comboBoxCategories->addItem(label);
  if (this->Internal->ui.comboBoxCategories->findText("Categories") != -1)
  {
    this->Internal->ui.comboBoxCategories->removeItem(
      this->Internal->ui.comboBoxCategories->findText("Categories"));
  }
  this->Internal->ui.editLabel->clear();
  this->Internal->ui.comboBoxCategories->setCurrentText(label);
}

//-------------------------------------------------------------------------
void ParalabelDockPanel::removeLabel()
{
  QString label = this->Internal->ui.comboBoxCategories->currentText();
  if (label.isEmpty())
  {
    std::cerr << "No label selected" << std::endl;
    return;
  }
  this->Internal->ui.comboBoxCategories->removeItem(this->Internal->ui.comboBoxCategories->findText(label));
}

//-------------------------------------------------------------------------
void ParalabelDockPanel::removeCategoryFromRepresentation(pqRepresentation* rep)
{
  pqDataRepresentation* representation = dynamic_cast<pqDataRepresentation*>(rep);
  if (representation == nullptr)
  {
    return;
  }
  
  pqPipelineSource* source = representation->getInput();
  if (source == nullptr)
  {
    return;
  }
  
  vtkSMSourceProxy* sourceProxy = vtkSMSourceProxy::SafeDownCast(source->getProxy());
  vtkSMOutputPort* outputPort = sourceProxy->GetOutputPort(static_cast<unsigned int>(0));
  vtkPVDataInformation* dataInfo = outputPort->GetDataInformation();
  if (dataInfo == nullptr)
  {
    return;
  }

  vtkDataAssembly* dataAssembly = dataInfo->GetDataAssembly();
  if (dataAssembly)
  {
    for (int i = 0; i < dataAssembly->GetNumberOfChildren(0); i++)
    {
      int node = dataAssembly->GetChild(0, i);
      const char* name = dataAssembly->GetNodeName(node);
      QString nameString = QString::fromStdString(name);
      if (this->Internal->ui.comboBoxCategories->findText(nameString) != -1)
      {
        this->Internal->ui.comboBoxCategories->removeItem(
          this->Internal->ui.comboBoxCategories->findText(nameString));
      }
    }
  }
  else
  {
    std::cerr << "No data assembly found, it's a annotation cube" << std::endl;
    QString label = vtkSMPropertyHelper(sourceProxy, "Label").GetAsString();
    if (this->Internal->ui.comboBoxCategories->findText(label) != -1)
    {
      this->Internal->ui.comboBoxCategories->removeItem(
        this->Internal->ui.comboBoxCategories->findText(label));
    }
  }
}

//-------------------------------------------------------------------------------
void ParalabelDockPanel::onProxyAdded()
{
  pqDataRepresentation* activeRepresentation = pqActiveObjects::instance().activeRepresentation();
  if (activeRepresentation == nullptr)
  {
    return;
  }

  pqPipelineSource* activeSource = activeRepresentation->getInput();
  if (activeSource == nullptr)
  {
    return;
  }

  vtkSMSourceProxy* sourceProxy = vtkSMSourceProxy::SafeDownCast(activeSource->getProxy());
  vtkSMOutputPort* outputPort = sourceProxy->GetOutputPort(static_cast<unsigned int>(0));
  vtkPVDataInformation* dataInfo = outputPort->GetDataInformation();
  if (dataInfo == nullptr)
  {
    return;
  }

  vtkDataAssembly* dataAssembly = dataInfo->GetDataAssembly();
  if (dataAssembly == nullptr)
  {
    return;
  }

  for (int i = 0; i < this->Internal->ui.comboBoxCategories->count(); i++)
  {
    if (this->Internal->ui.comboBoxCategories->itemText(i) == "Categories")
    {
      this->Internal->ui.comboBoxCategories->removeItem(i);
    }
  }
  for (int i = 0; i < dataAssembly->GetNumberOfChildren(0); i++)
  {
    int node = dataAssembly->GetChild(0, i);
    const char* name = dataAssembly->GetNodeName(node);
    QString nameString = QString::fromStdString(name);
    if (this->Internal->ui.comboBoxCategories->findText(nameString) != -1)
    {
      continue;
    }

    this->Internal->ui.comboBoxCategories->addItem(nameString);
  }
}

//-------------------------------------------------------------------------
void ParalabelDockPanel::enableBoxWidgets(bool enable)
{
  for (auto& widget : this->widgets)
  {
    widget->SetEnabled(enable);
  }
}
