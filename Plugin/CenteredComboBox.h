// SPDX-FileCopyrightText: Copyright (c) Kitware Inc.
// SPDX-FileCopyrightText: Copyright (c) Sandia Corporation
// SPDX-License-Identifier: BSD-3-Clause
/**
 * @class CenteredComboBox
 * @brief This class is used to center the text in a combobox
*/

#ifndef CenteredComboBox_h
#define CenteredComboBox_h

#include <QComboBox>

class CenteredComboBox : public QComboBox
{
    Q_OBJECT
public:
    explicit CenteredComboBox(QWidget *parent = nullptr) : QComboBox(parent) {}

protected:
    void paintEvent(QPaintEvent *event) override;
};

#endif
