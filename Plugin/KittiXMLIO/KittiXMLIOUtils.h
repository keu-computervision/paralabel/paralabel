/*=========================================================================
  SPDX-FileCopyrightText: Copyright (c) Kitware Inc.
  SPDX-License-Identifier: Apache-2.0
=========================================================================*/
/**
 * @file KittiXMLIOUtils.h
 * @section Description
 * This file contains a namespace with strcutures and functions used to read and write Kitti XML files.
 * The structure KittiXMLInfo is used to store the information of each annotation.
 * The structure KittiXMLLabels is used to store the labels of the XML elements.
 * The other functions are used to read and write the XML file.
*/

#ifndef KittiXMLIOUtils_h
#define KittiXMLIOUtils_h

#include <vtkFieldData.h>
#include <vtkFloatArray.h>
#include <vtkPolyData.h>
#include <vtkStringArray.h>
#include <vtkXMLDataElement.h>
#include <vtkTransform.h>

namespace KittiXMLIOUtils
{
  // Store the information of each annotation, contains the label of the annotation, the scale, the position, the rotation, and the bounds
  struct KittiXMLInfo
  {
    const char* nameLabel;
    union 
    {
      struct
      {
        float scaleX;
        float scaleY;
        float scaleZ;
      };
      float scale3D[3]; // [scaleX, scaleY, scaleZ]
      
    };
    union 
    {
      struct
      {
        float posX;
        float posY;
        float posZ;
      };
      float position[3]; // [posX, posY, posZ]
    };
    union 
    {
      struct
      {
        float rotX;
        float rotY;
        float rotZ;
      };
      float rotationXYZ[3]; // [rotX, rotY, rotZ]
    };
    union
    {
      struct
      {
        float quatW;
        float quatX;
        float quatY;
        float quatZ;
      };
      float rotationQuat[4]; // [quatW, quatX, quatY, quatZ]
    };
    union
    {
      struct
      {
        float minX;
        float maxX;
        float minY;
        float maxY;
        float minZ;
        float maxZ;
      };
      float bounds[6]; // [minX, maxX, minY, maxY, minZ, maxZ]
    };
    union 
    {
      struct
      {
        double meanX;
        double meanY;
        double meanZ;
      };
      double mean[3]; // [meanX, meanY, meanZ]
    };
  };
  
  // Store the labels of the XML elements to get and set values in elements of the XML file
  struct KittiXMLLabels
  {
    // labels for Kitti XML elements
    static constexpr const char* objectTypeLabel = "objectType";
    static constexpr const char* scaleXLabel = "h";
    static constexpr const char* scaleYLabel = "w";
    static constexpr const char* scaleZLabel = "l";
    static constexpr const char* posXLabel = "tx";
    static constexpr const char* posYLabel = "ty";
    static constexpr const char* posZLabel = "tz";
    static constexpr const char* rotXLabel = "rx";
    static constexpr const char* rotYLabel = "ry";
    static constexpr const char* rotZLabel = "rz";
    static constexpr const char* quatWLabel = "qw";
    static constexpr const char* quatXLabel = "qx";
    static constexpr const char* quatYLabel = "qy";
    static constexpr const char* quatZLabel = "qz";
    static constexpr const char* posesLabel = "poses";
    static constexpr const char* countLabel = "count";
    static constexpr const char* itemLabel = "item";
    static constexpr const char* itemVersionLabel = "item_version";
    static constexpr const char* rootLabel = "Root";
    static constexpr const char* stateLabel = "state";
    static constexpr const char* occlusionLabel = "occlusion";
    static constexpr const char* occlusionKFLabel = "occlusion_kf";
    static constexpr const char* truncationLabel = "truncation";
    static constexpr const char* amtOcclusionLabel = "amt_occlusion";
    static constexpr const char* amtBorderLLabel = "amt_border_l";
    static constexpr const char* amtBorderRLabel = "amt_border_r";
    static constexpr const char* amtOcclusionKFLabel = "amt_occlusion_kf";
    static constexpr const char* amtBorderKFLabel = "amt_border_kf";
    static constexpr const char* firstFrameLabel = "first_frame";
    static constexpr const char* finishedLabel = "finished";
    static constexpr const char* boostSerializationLabel = "boost_serialization";
    static constexpr const char* versionLabel = "version";
    static constexpr const char* signatureLabel = "signature";
    static constexpr const char* trackletsLabel = "tracklets";

    // labels for field data
    static constexpr const char* labelLabel = "label";
    static constexpr const char* uidLabel = "uid";
    static constexpr const char* positionLabel = "position";
    static constexpr const char* rotationXYZLabel = "rotationXYZ";
    static constexpr const char* scaleLabel = "scale";
    static constexpr const char* rotationQuaternionLabel = "rotationQuat";
  };
  
  //----------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Check if the item has all the necessary information to be considered valid
   * called before creating the polydata
  */
  bool CheckingItemInformation(vtkXMLDataElement* item);

  //----------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Remove the spaces from a string, called in order to remove the spaces from the label of the annotations
   * because of the method used to create the XML file PrintXML()
   */ 
  std::string RemoveSpaces(const std::string& input);
  
  //----------------------------------------------------------------------------------------------------------------------------------------
  /**
   * All these functions are used to create an XML element to add to the root of the XML file
   * given a name and a value and add it to the parent
   * called during the creation of the XML file (write_data)
  */
  void CreateAndAddXMLElement(const char* name, const std::string& value, vtkXMLDataElement* parent);
  // Create the XML element for the position and rotation of the annotation
  vtkXMLDataElement* CreatePositionRotationMember(double* bounds, vtkPolyData* polydata, KittiXMLLabels labelStruct);
  // Create the XML element for the scale of the annotation
  vtkXMLDataElement* CreateItemMember(int i, vtkPolyData* polydata, KittiXMLLabels labelStruct);
  // Create the XML element "poses" to respect the Kitti 1.0 format
  vtkXMLDataElement* CreatePosesMember(double* bounds, vtkPolyData* polydata, KittiXMLLabels labelStruct);

  //----------------------------------------------------------------------------------------------------------------------------------------
  // Get the value of a element in the XML file and convert it to a float, called during GetItemData()
  float GetNestedElementValue(vtkXMLDataElement* parent, const char* name);

  //----------------------------------------------------------------------------------------------------------------------------------------
  // Get the data of one annotation at a time from the XML file, called before the creation of the polydata from the item
  void GetIemData(vtkXMLDataElement* item, KittiXMLInfo& annotationInfo, KittiXMLLabels labelStruct);
  
  //----------------------------------------------------------------------------------------------------------------------------------------
  // These functions are used to create the polydata from the KittiXMLInfo structure 
  void ApplyRotationXYZ(vtkPoints* points, KittiXMLInfo& annotationInfo);
  //----------------------------------------------------------------------------------------------------------------------------------------
  void ApplyQuaternion(vtkPoints* points, KittiXMLInfo& annotationInfo);
  //----------------------------------------------------------------------------------------------------------------------------------------
  void InsertPoint(vtkPoints* points, KittiXMLInfo& annotationInfo);
  //----------------------------------------------------------------------------------------------------------------------------------------
  void CreateLinesOfCuboid(vtkPoints* points, vtkPolyData* polydata);
  //----------------------------------------------------------------------------------------------------------------------------------------
  vtkPolyData* CreatePolydataFromItem(vtkXMLDataElement* item, vtkPoints* points, vtkPolyData* polydata, KittiXMLInfo& annotationInfo, KittiXMLLabels labelStruct);
  //----------------------------------------------------------------------------------------------------------------------------------------
  void TransformPoints(vtkPoints* points, vtkTransform* transform, double center[3]);

  //----------------------------------------------------------------------------------------------------------------------------------------
  // These functions are used to add field data informations to the polydata, called during the creation of the polydata from the item  
  template <typename ArrayType, typename ValueType>
  void AddArrayToPolyData(vtkPolyData* polydata, const char* name, ValueType value);
  void SetFieldDataToPolydata(vtkPolyData* polydata, const char* nameLabel, int nodeId, KittiXMLInfo& annotationInfo, KittiXMLLabels labelStruct);
  void ModifyFieldData(vtkFieldData* fieldData, KittiXMLInfo& annotationInfo, KittiXMLLabels labelStruct);
  void GetValuesOfPolydata(vtkPolyData* polydata, KittiXMLInfo& annotationInfo);
  void GetScaleFromOrientedBBox(vtkPoints* pointsWithoutRotation, double scale[3]);
  void GetQuaterionFromOrientedBBox(vtkPolyData* polyData, vtkPoints* pointsWithoutRotation, double center[3], double length[3], double quat[4]);
}

#endif
