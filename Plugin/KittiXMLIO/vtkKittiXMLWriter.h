/*=========================================================================
  SPDX-FileCopyrightText: Copyright (c) Kitware Inc.
  SPDX-License-Identifier: Apache-2.0
=========================================================================*/
/**
 * @class vtkKittiXMLWriter - sample writer
 * .SECTION Description
 * vtkKittiXMLWriter is a sample writer imported in ParaView. It writes data to a XML file (format Kitti 1.0) from a vtkPartitionedDataSetCollection.
 * The vtkPartitionedDataSetCollection contains a vtkDataAssembly to explicit the structure of the data and N vtkPartitionedDatasets where N is the number
*/

#ifndef vtkKittiXMLWriter_h
#define vtkKittiXMLWriter_h

#include "vtkKittiXMLIOModule.h"
#include <vtkWriter.h>

class vtkPartitionedDataSetCollection;

class VTKKITTIXMLIO_EXPORT vtkKittiXMLWriter : public vtkWriter
{
public:
  static vtkKittiXMLWriter* New();
  vtkTypeMacro(vtkKittiXMLWriter, vtkWriter);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  //@{
  // Description: Specify file name of the Kitti XML file to write.
  vtkSetStringMacro(FileName);
  vtkGetStringMacro(FileName);
  //@}

  vtkPartitionedDataSetCollection* GetInput();

protected:
    vtkKittiXMLWriter();
    ~vtkKittiXMLWriter() override;
    
    int RequestData(vtkInformation*, vtkInformationVector**, vtkInformationVector*);
    void WriteData() override;
    int FillInputPortInformation(int, vtkInformation*) override;

private:
    vtkKittiXMLWriter(const vtkKittiXMLWriter&) = delete;
    void operator=(const vtkKittiXMLWriter&) = delete;
    char* FileName = nullptr;
};

#endif
