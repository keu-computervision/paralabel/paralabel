/*=========================================================================
  SPDX-FileCopyrightText: Copyright (c) Kitware Inc.
  SPDX-License-Identifier: Apache-2.0
=========================================================================*/
/**
 * @file TestUtils.h
 * @section Description
 * This file contains a namespace with a function to calculate the SHA512 hash of a file.
 * This hash file generated temporary is compared with a hash reference file to check test results.
 */

#ifndef TestUtils_h
#define TestUtils_h

#include <openssl/sha.h>
#include <fstream>
#include <iostream>
#include <iomanip>

namespace TestUtils
{
    std::string sha512(const std::string& filename) 
    {
        std::ifstream file(filename, std::ifstream::binary);
        if (!file) 
        {
            std::cerr << "Failed to open file: " << filename << std::endl;
            return "";
        }

        SHA512_CTX sha512Context;
        SHA512_Init(&sha512Context);

        const size_t bufferSize = 8192;
        char buffer[bufferSize];
        while (file.good()) 
        {
            file.read(buffer, bufferSize);
            SHA512_Update(&sha512Context, buffer, file.gcount());
        }

        unsigned char hash[SHA512_DIGEST_LENGTH];
        SHA512_Final(hash, &sha512Context);
        
        std::stringstream ss;
        for (int i = 0; i < SHA512_DIGEST_LENGTH; ++i) 
        {
            ss << std::hex << std::setw(2) << std::setfill('0') << static_cast<int>(hash[i]);
        }
        return ss.str();
    }
}

#endif
