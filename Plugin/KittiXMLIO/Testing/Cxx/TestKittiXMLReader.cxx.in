#include "vtkKittiXMLReader.h"
#include "vtkPolyData.h"
#include "vtkNew.h"
#include "vtkXMLPartitionedDataSetCollectionReader.h"
#include "vtkPartitionedDataSetCollection.h"
#include "vtkPartitionedDataSet.h"
#include "vtkTestUtilities.h"
#include "vtkFieldData.h"
#include "vtkAbstractArray.h"
#include <filesystem>
#include "openssl/sha.h"


int TestKittiXMLReader_@test_name@(int argc, char* argv[])
{
    std::filesystem::path testDirectoryData = "@PATH_KITTIXML_READER_WRITER_TESTS@";
    std::filesystem::path kittiXMLFile = testDirectoryData / "Input/test_ahn4.xml";
    vtkNew<vtkKittiXMLReader> kittiXMLReader;
    kittiXMLReader->SetFileName(kittiXMLFile.c_str());
    kittiXMLReader->Update();
    vtkSmartPointer<vtkPartitionedDataSetCollection> outputVTPC = vtkPartitionedDataSetCollection::SafeDownCast(kittiXMLReader->GetOutput());
    
    std::filesystem::path expectedKittiVTPC = testDirectoryData / "Expected/test_KittiXML.vtpc"; 
    vtkNew<vtkXMLPartitionedDataSetCollectionReader> expectedReader;
    expectedReader->SetFileName(expectedKittiVTPC.c_str());
    expectedReader->Update();
    vtkSmartPointer<vtkPartitionedDataSetCollection> expectedVTPC = vtkPartitionedDataSetCollection::SafeDownCast(expectedReader->GetOutput());

    if (!outputVTPC || !expectedVTPC)
    {
        std::cerr << "Output or expected is null" << std::endl;
        return EXIT_FAILURE;
    }
    if (outputVTPC->GetNumberOfPartitionedDataSets() != expectedVTPC->GetNumberOfPartitionedDataSets())
    {
        std::cerr << "Number of partitions does not match" << std::endl;
        return EXIT_FAILURE;
    }

    for(int i=0; i<outputVTPC->GetNumberOfPartitionedDataSets(); i++)
    {
        vtkSmartPointer<vtkPartitionedDataSet> currentOutputPartition = vtkPartitionedDataSet::SafeDownCast(outputVTPC->GetPartitionedDataSet(i));
        vtkSmartPointer<vtkPartitionedDataSet> currentExpectedPartition = vtkPartitionedDataSet::SafeDownCast(expectedVTPC->GetPartitionedDataSet(i));
        vtkSmartPointer<vtkPolyData> outputPolyData = vtkPolyData::SafeDownCast(currentOutputPartition->GetPartition(0));
        vtkSmartPointer<vtkPolyData> expectedPolyData = vtkPolyData::SafeDownCast(currentExpectedPartition->GetPartition(0));

        if(!outputPolyData || !expectedPolyData)
        {
            std::cerr << "Polydata from partition Dataset " << i << " is null" << std::endl;
            return EXIT_FAILURE;
        }
        
        for (int j = 0; j < outputPolyData->GetNumberOfPoints(); j++)
        {
            double pointOutput[3];
            double pointExpected[3];
            outputPolyData->GetPoint(j, pointOutput);
            expectedPolyData->GetPoint(j, pointExpected);

            if (pointOutput[0] != pointExpected[0] || pointOutput[1] != pointExpected[1] || pointOutput[2] != pointExpected[2])
            {
                std::cerr << "Point " << j << " of the PolyData " << i << " does not match with the point " << j << " of the Expected Polydata" << std::endl;
                return EXIT_FAILURE;
            }            
        }

        if(outputPolyData->GetFieldData()->GetNumberOfArrays() != expectedPolyData->GetFieldData()->GetNumberOfArrays())
        {
            std::cout << outputPolyData->GetFieldData()->GetNumberOfArrays() << std::endl;
            std::cout << expectedPolyData->GetFieldData()->GetNumberOfArrays() << std::endl;
            std::cerr << "Number of arrays does not match" << std::endl;
            return EXIT_FAILURE;
        }

        for(int f=0; f<outputPolyData->GetFieldData()->GetNumberOfArrays(); f++)
        {
            auto outputArray = outputPolyData->GetFieldData()->GetAbstractArray(f);
            auto expectedArray = expectedPolyData->GetFieldData()->GetAbstractArray(f);
            if(!outputArray || !expectedArray)
            {
                std::cerr << "Array " << f << " is null" << std::endl;
                return EXIT_FAILURE;
            }
            if(outputArray->GetNumberOfTuples() != expectedArray->GetNumberOfTuples())
            {
                std::cerr << "Number of tuples does not match" << std::endl;
                return EXIT_FAILURE;
            }
            for(int t=0; t<outputArray->GetNumberOfTuples(); t++)
            {
                if(outputArray->GetVariantValue(t) != expectedArray->GetVariantValue(t))
                {
                    std::cerr << "Value " << t << " does not match" << std::endl;
                    return EXIT_FAILURE;
                }
            }
        }
    }

    return EXIT_SUCCESS;
}
