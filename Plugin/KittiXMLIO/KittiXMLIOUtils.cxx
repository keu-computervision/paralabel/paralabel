#include "KittiXMLIOUtils.h"

#include <vtkTransform.h>
#include <vtkPolyLine.h>
#include <vtkLandmarkTransform.h>

namespace KittiXMLIOUtils
{
  //--------------------------------------------------------------------------------
  bool CheckingItemInformation(vtkXMLDataElement* item)
  {
    std::vector<std::string> itemElements = { "objectType", "h", "w", "l", "poses" };
    bool isValidItem = true;

    for (const auto& element : itemElements) 
    {
      if (item->FindNestedElementWithName(element.c_str()) == nullptr) 
      {
        isValidItem = false;
        break;
      }
    }

    if (isValidItem) 
    {
      vtkXMLDataElement* poses = item->FindNestedElementWithName("poses");
      if (poses->FindNestedElementWithName("item") != nullptr) 
      {
        vtkXMLDataElement* pose = poses->FindNestedElementWithName("item");
        std::vector<std::string> poseElements = { "tx", "ty", "tz", "rx", "ry", "rz" };
        for (const auto& element : poseElements) 
        {
          if (pose->FindNestedElementWithName(element.c_str()) == nullptr) 
          {
            isValidItem = false;
            break;
          }
        }
      } 
      else 
      {
        isValidItem = false;
      }
    }

    return isValidItem;
  };

  //--------------------------------------------------------------------------------
  void CreateAndAddXMLElement(const char* name, const std::string& value, vtkXMLDataElement* parent)
  {
    vtkSmartPointer<vtkXMLDataElement> element = vtkSmartPointer<vtkXMLDataElement>::New();
    element->SetName(name);
    element->SetCharacterData(value.c_str(), value.length());
    parent->AddNestedElement(element);
  };

  //--------------------------------------------------------------------------------
  std::string RemoveSpaces(const std::string& input)
  {
    std::string strInput = input;
    strInput.erase(std::remove_if(strInput.begin(), strInput.end(), ::isspace), strInput.end());
    return strInput;
  };

  //--------------------------------------------------------------------------------
  float GetNestedElementValue(vtkXMLDataElement* element, const char* name)
  {
    //check if the element exists
    if (element->FindNestedElementWithName(name) == nullptr) 
    {
      return 0;
    }
    return std::stof(element->FindNestedElementWithName(name)->GetCharacterData());
  };

  //--------------------------------------------------------------------------------
  void ApplyRotationXYZ(vtkPoints* points, KittiXMLInfo& annotationInfo) //rotation of angles between -180 and 180 degrees
  {
    vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();
    transform->RotateX(annotationInfo.rotX);
    transform->RotateY(annotationInfo.rotY);
    transform->RotateZ(annotationInfo.rotZ);
    TransformPoints(points, transform, annotationInfo.mean);
  };

  //--------------------------------------------------------------------------------
  void ApplyQuaternion(vtkPoints* points, KittiXMLInfo& annotationInfo)
  {
    vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();
    transform->RotateWXYZ(annotationInfo.quatW, annotationInfo.quatX, annotationInfo.quatY, annotationInfo.quatZ);
    TransformPoints(points, transform, annotationInfo.mean);
  };

  //--------------------------------------------------------------------------------
  vtkXMLDataElement* CreatePositionRotationMember(double* bounds, vtkPolyData* polydata, KittiXMLLabels labelStruct)
  {
    vtkXMLDataElement* posRotMember = vtkXMLDataElement::New();
    posRotMember->SetName(labelStruct.itemLabel);
    double rotation[3];
    double quaternion[4];

    if (polydata->GetFieldData()->GetArray(labelStruct.rotationQuaternionLabel) == nullptr) 
    {
      for (int i = 0; i < 4; i++) 
      {
        quaternion[i] = 0;
      }
    }
    else
    {
      auto quat = polydata->GetFieldData()->GetArray(labelStruct.rotationQuaternionLabel)->GetTuple4(0);
      for (int i = 0; i < 4; i++) 
      {
        quaternion[i] = quat[i];
      }
    }

    if (polydata->GetFieldData()->GetArray(labelStruct.rotationXYZLabel) == nullptr) 
    {
      for (int i=0; i<3; i++)
      {
        rotation[i] = 0;
      }
    }
    else
    {
      auto rotation = polydata->GetFieldData()->GetArray(labelStruct.rotationXYZLabel)->GetTuple3(0);
    }

    auto position = polydata->GetFieldData()->GetArray(labelStruct.positionLabel)->GetTuple3(0);
    std::vector<std::pair<const char*, std::string>> listElements = {
      { labelStruct.posXLabel, std::to_string(position[0]) },
      { labelStruct.posYLabel, std::to_string(position[1]) },
      { labelStruct.posZLabel, std::to_string(position[2]) },
      { labelStruct.rotXLabel, std::to_string(rotation[0]) },
      { labelStruct.rotYLabel, std::to_string(rotation[1]) },
      { labelStruct.rotZLabel, std::to_string(rotation[2]) },
      { labelStruct.quatWLabel, std::to_string(quaternion[0]) },
      { labelStruct.quatXLabel, std::to_string(quaternion[1]) },
      { labelStruct.quatYLabel, std::to_string(quaternion[2]) },
      { labelStruct.quatZLabel, std::to_string(quaternion[3]) },
      { labelStruct.stateLabel, "2" },
      { labelStruct.occlusionLabel, "0" },
      { labelStruct.occlusionKFLabel, "0" },
      { labelStruct.truncationLabel, "0" },
      { labelStruct.amtOcclusionLabel, "-1" },
      { labelStruct.amtBorderLLabel, "-1" },
      { labelStruct.amtBorderRLabel, "-1" },
      { labelStruct.amtOcclusionKFLabel, "-1" },
      { labelStruct.amtBorderKFLabel, "-1" }
    };

    for (const auto& element : listElements) 
    {
      CreateAndAddXMLElement(element.first, element.second, posRotMember);
    }

    return posRotMember;
  };

  //--------------------------------------------------------------------------------
  vtkXMLDataElement* CreatePosesMember(double* bounds, vtkPolyData* polydata, KittiXMLLabels labelStruct)
  {
    vtkXMLDataElement* poses = vtkXMLDataElement::New();
    poses->SetName(labelStruct.posesLabel);
    CreateAndAddXMLElement(labelStruct.countLabel, "1", poses);
    CreateAndAddXMLElement(labelStruct.itemVersionLabel, "0", poses);
    vtkSmartPointer<vtkXMLDataElement> posRotMember = vtkSmartPointer<vtkXMLDataElement>::New();
    posRotMember.TakeReference(CreatePositionRotationMember(bounds, polydata, labelStruct)); // adding the member containing the position and rotation of the annotation / bounding box
    poses->AddNestedElement(posRotMember);
    return poses;
  }; 

  //--------------------------------------------------------------------------------
  vtkXMLDataElement* CreateItemMember(int i, vtkPolyData* polydata, KittiXMLLabels labelStruct)
  {
    vtkXMLDataElement* item = vtkXMLDataElement::New();
    item->SetName(labelStruct.itemLabel);

    auto label = polydata->GetFieldData()->GetAbstractArray(labelStruct.labelLabel)->GetVariantValue(0).ToString();
    CreateAndAddXMLElement(labelStruct.objectTypeLabel, label, item);

    double* scales = polydata->GetFieldData()->GetArray(labelStruct.scaleLabel)->GetTuple(0);
    std::vector<const char*> scaleLabels = { labelStruct.scaleXLabel, labelStruct.scaleYLabel, labelStruct.scaleZLabel };
    for (int scaleIndex = 0; scaleIndex < scaleLabels.size(); scaleIndex++) 
    {
      CreateAndAddXMLElement(scaleLabels[scaleIndex], std::to_string(scales[scaleIndex]), item);
    }

    CreateAndAddXMLElement(labelStruct.firstFrameLabel, "0", item);

    double* bounds = polydata->GetBounds();
    vtkSmartPointer<vtkXMLDataElement> poses = vtkSmartPointer<vtkXMLDataElement>::New();
    poses.TakeReference(CreatePosesMember(bounds, polydata, labelStruct)); // adding the member containing the PosRotMember and also giving some information about the annotation
    item->AddNestedElement(poses);
    CreateAndAddXMLElement(labelStruct.finishedLabel, "1", item);
    return item;
  };

  //--------------------------------------------------------------------------------
  void GetItemData(vtkXMLDataElement* item, KittiXMLIOUtils::KittiXMLInfo& annotationInfo, KittiXMLIOUtils::KittiXMLLabels labelStruct)
  {
    std::vector<std::pair<float*, const char*>> scaleElements = {
      { &annotationInfo.scaleX, labelStruct.scaleXLabel },
      { &annotationInfo.scaleY, labelStruct.scaleYLabel },
      { &annotationInfo.scaleZ, labelStruct.scaleZLabel }
    };

    for (const auto& element : scaleElements) 
    {
      *element.first = GetNestedElementValue(item, element.second);
    }

    vtkSmartPointer<vtkXMLDataElement> poses = item->FindNestedElementWithName(labelStruct.posesLabel);
    vtkSmartPointer<vtkXMLDataElement> pose = poses->FindNestedElementWithName(labelStruct.itemLabel);
    std::vector<std::pair<float*, const char*>> poseElements = {
      { &annotationInfo.posX, labelStruct.posXLabel },
      { &annotationInfo.posY, labelStruct.posYLabel },
      { &annotationInfo.posZ, labelStruct.posZLabel },
      { &annotationInfo.rotX, labelStruct.rotXLabel },
      { &annotationInfo.rotY, labelStruct.rotYLabel },
      { &annotationInfo.rotZ, labelStruct.rotZLabel },
      { &annotationInfo.quatW, labelStruct.quatWLabel },
      { &annotationInfo.quatX, labelStruct.quatXLabel },
      { &annotationInfo.quatY, labelStruct.quatYLabel },
      { &annotationInfo.quatZ, labelStruct.quatZLabel}
    };

    for (const auto& element : poseElements) 
    {
      *element.first = GetNestedElementValue(pose, element.second);
    }
    annotationInfo.minX = annotationInfo.posX - annotationInfo.scaleX / 2;
    annotationInfo.maxX = annotationInfo.posX + annotationInfo.scaleX / 2;
    annotationInfo.minY = annotationInfo.posY - annotationInfo.scaleY / 2;
    annotationInfo.maxY = annotationInfo.posY + annotationInfo.scaleY / 2;
    annotationInfo.minZ = annotationInfo.posZ - annotationInfo.scaleZ / 2;
    annotationInfo.maxZ = annotationInfo.posZ + annotationInfo.scaleZ / 2;
    annotationInfo.mean[0] = (annotationInfo.minX + annotationInfo.maxX) / 2;
    annotationInfo.mean[1] = (annotationInfo.minY + annotationInfo.maxY) / 2;
    annotationInfo.mean[2] = (annotationInfo.minZ + annotationInfo.maxZ) / 2;
  };

  //--------------------------------------------------------------------------------
  void InsertPoint(vtkPoints* points, KittiXMLIOUtils::KittiXMLInfo& annotationInfo)
  {
    points->InsertNextPoint(annotationInfo.minX, annotationInfo.minY, annotationInfo.minZ);
    points->InsertNextPoint(annotationInfo.maxX, annotationInfo.minY, annotationInfo.minZ);
    points->InsertNextPoint(annotationInfo.maxX, annotationInfo.maxY, annotationInfo.minZ);
    points->InsertNextPoint(annotationInfo.minX, annotationInfo.maxY, annotationInfo.minZ);
    points->InsertNextPoint(annotationInfo.minX, annotationInfo.minY, annotationInfo.maxZ);
    points->InsertNextPoint(annotationInfo.maxX, annotationInfo.minY, annotationInfo.maxZ);
    points->InsertNextPoint(annotationInfo.maxX, annotationInfo.maxY, annotationInfo.maxZ);
    points->InsertNextPoint(annotationInfo.minX, annotationInfo.maxY, annotationInfo.maxZ);
  };

  //--------------------------------------------------------------------------------
  void CreateLinesOfCuboid(vtkPoints* points, vtkPolyData* polydata)
  {
    vtkSmartPointer<vtkCellArray> lines = vtkSmartPointer<vtkCellArray>::New();
    vtkSmartPointer<vtkPolyLine> polyLine = vtkSmartPointer<vtkPolyLine>::New();

    // Define the pairs of points to connect
    int pointPairs[12][2] = {
      { 0, 1 }, { 1, 2 }, { 2, 3 }, { 3, 0 }, // bottom face
      { 4, 5 },
      { 5, 6 },
      { 6, 7 },
      { 7, 4 }, // top face
      { 0, 4 },
      { 1, 5 },
      { 2, 6 },
      { 3, 7 } // vertical edges
    };

    for (int i = 0; i < 12; ++i) 
    {
      polyLine->GetPointIds()->SetNumberOfIds(2);
      polyLine->GetPointIds()->SetId(0, pointPairs[i][0]);
      polyLine->GetPointIds()->SetId(1, pointPairs[i][1]);
      lines->InsertNextCell(polyLine);
    }
    polydata->SetLines(lines);
  };

  //--------------------------------------------------------------------------------
  vtkPolyData* CreatePolydataFromItem(vtkXMLDataElement* item, vtkPoints* points, vtkPolyData* polydata, KittiXMLIOUtils::KittiXMLInfo& annotationInfo, KittiXMLIOUtils::KittiXMLLabels labelStruct)
  {
    GetItemData(item, annotationInfo, labelStruct);
    InsertPoint(points, annotationInfo);
    if (annotationInfo.quatW != 0 || annotationInfo.quatX != 0 || annotationInfo.quatY != 0 || annotationInfo.quatZ != 0) 
    {
      KittiXMLIOUtils::ApplyQuaternion(points, annotationInfo);
    }
    else if (annotationInfo.rotX != 0 || annotationInfo.rotY != 0 || annotationInfo.rotZ != 0) 
    {
      KittiXMLIOUtils::ApplyRotationXYZ(points, annotationInfo);
    }

    vtkSmartPointer<vtkCellArray> cell = vtkSmartPointer<vtkCellArray>::New();
    int nb_points = points->GetNumberOfPoints();
    cell->InsertNextCell(nb_points);
    for (int i = 0; i < nb_points; i++) 
    {
      cell->InsertCellPoint(i);
    }
    polydata->SetPoints(points);
    polydata->SetVerts(cell);
    CreateLinesOfCuboid(points, polydata);

    return polydata;
  };

  //--------------------------------------------------------------------------------
  void TransformPoints(vtkPoints* points, vtkTransform* transform, double center[3])
  {
    double point[3];
    for (int i = 0; i < points->GetNumberOfPoints(); i++) 
    {
      points->GetPoint(i, point);
      std::transform(point, point + (std::end(point) - std::begin(point)), center, point, std::minus<double>());
      transform->TransformPoint(point, point);
      std::transform(point, point + (std::end(point) - std::begin(point)), center, point, std::plus<double>());
      points->SetPoint(i, point);
    }
  }

  //--------------------------------------------------------------------------------
  template <typename ArrayType, typename ValueType>
  void AddArrayToPolyData(vtkPolyData* polydata, const char* name, ValueType value)
  {
    vtkSmartPointer<ArrayType> array = vtkSmartPointer<ArrayType>::New();
    array->SetName(name);
    array->InsertNextValue(value);
    polydata->GetFieldData()->AddArray(array);
  };

  //--------------------------------------------------------------------------------
  void SetFieldDataToPolydata(vtkPolyData* polydata, const char* nameLabel, int nodeId, KittiXMLInfo& annotationInfo, KittiXMLLabels labelStruct)
  {
    AddArrayToPolyData<vtkStringArray, const char*>(polydata, labelStruct.labelLabel, nameLabel);
    AddArrayToPolyData<vtkIdTypeArray, vtkIdType>(polydata, labelStruct.uidLabel, nodeId);

    std::vector<std::pair<const char*, std::array<float, 3>>> floatArrays = {
      { labelStruct.positionLabel, { annotationInfo.posX, annotationInfo.posY, annotationInfo.posZ } },
      { labelStruct.rotationXYZLabel, { annotationInfo.rotX, annotationInfo.rotY, annotationInfo.rotZ } },
      { labelStruct.scaleLabel, { annotationInfo.scaleX, annotationInfo.scaleY, annotationInfo.scaleZ } }
    };

    for (const auto& [name, values] : floatArrays) 
    {
      vtkSmartPointer<vtkFloatArray> array = vtkSmartPointer<vtkFloatArray>::New();
      array->SetNumberOfComponents(3);
      array->SetNumberOfTuples(1);
      array->SetName(name);
      array->SetTuple3(0, values[0], values[1], values[2]);
      polydata->GetFieldData()->AddArray(array);
    }
    vtkSmartPointer<vtkFloatArray> quaternionArray = vtkSmartPointer<vtkFloatArray>::New();
    quaternionArray->SetNumberOfComponents(4);
    quaternionArray->SetNumberOfTuples(1);
    quaternionArray->SetName(labelStruct.rotationQuaternionLabel);
    quaternionArray->SetTuple4(0, annotationInfo.quatW, annotationInfo.quatX, annotationInfo.quatY, annotationInfo.quatZ);
    polydata->GetFieldData()->AddArray(quaternionArray);
  };

  //--------------------------------------------------------------------------------
  void ModifyFieldData(vtkFieldData* fieldData, KittiXMLInfo& annotationInfo, KittiXMLLabels labelStruct)
  {
    vtkDataArray* positionArray = fieldData->GetArray(labelStruct.positionLabel);
    positionArray->SetTuple3(0, annotationInfo.posX, annotationInfo.posY, annotationInfo.posZ);

    vtkDataArray* scaleArray = fieldData->GetArray(labelStruct.scaleLabel);
    scaleArray->SetTuple3(0, annotationInfo.scaleX, annotationInfo.scaleY, annotationInfo.scaleZ);

    vtkDataArray* rotationArray = fieldData->GetArray(labelStruct.rotationXYZLabel);
    rotationArray->SetTuple3(0, annotationInfo.rotX, annotationInfo.rotY, annotationInfo.rotZ);

    vtkDataArray* quaternionArray = fieldData->GetArray(labelStruct.rotationQuaternionLabel);
    quaternionArray->SetTuple4(0, annotationInfo.quatW, annotationInfo.quatX, annotationInfo.quatY, annotationInfo.quatZ);

    vtkStringArray* labelArray = vtkStringArray::SafeDownCast(fieldData->GetAbstractArray(labelStruct.labelLabel));
    labelArray->SetValue(0, annotationInfo.nameLabel);
  };

  //--------------------------------------------------------------------------------
  void GetValuesOfPolydata(vtkPolyData* polydata, KittiXMLInfo& annotationInfo)
  {
    double bounds[6];
    polydata->GetBounds(bounds);
    double center[3] = { (bounds[0] + bounds[1]) / 2, (bounds[2] + bounds[3]) / 2, (bounds[4] + bounds[5]) / 2 };
    double length[3];
    GetScaleFromOrientedBBox(polydata->GetPoints(), length);

    vtkSmartPointer<vtkPoints> pointsWithoutRotation = vtkSmartPointer<vtkPoints>::New();
    double quat[4] = { 0, 0, 0, 1 };
    GetQuaterionFromOrientedBBox(polydata, pointsWithoutRotation, center, length, quat);

    for (int i = 0; i < 6; i++) 
    {
      if (i < 3) 
      {
        annotationInfo.position[i] = static_cast<float>(center[i]);
        annotationInfo.scale3D[i] = static_cast<float>(length[i]);
        annotationInfo.rotationXYZ[i] = static_cast<float>(0);
        annotationInfo.mean[i] = static_cast<float>(center[i]);
      }
      if (i < 4) 
      {
        annotationInfo.rotationQuat[i] = static_cast<float>(quat[i]);
      }
      annotationInfo.bounds[i] = static_cast<float>(bounds[i]);
    }
  };

  //--------------------------------------------------------------------------------
  void GetScaleFromOrientedBBox(vtkPoints* points, double scale[3])
  {
    scale[0] = sqrt(pow(points->GetPoint(1)[0] - points->GetPoint(0)[0], 2) + pow(points->GetPoint(1)[1] - points->GetPoint(0)[1], 2) + pow(points->GetPoint(1)[2] - points->GetPoint(0)[2], 2));
    scale[1] = sqrt(pow(points->GetPoint(3)[0] - points->GetPoint(0)[0], 2) + pow(points->GetPoint(3)[1] - points->GetPoint(0)[1], 2) + pow(points->GetPoint(3)[2] - points->GetPoint(0)[2], 2));
    scale[2] = sqrt(pow(points->GetPoint(4)[0] - points->GetPoint(0)[0], 2) + pow(points->GetPoint(4)[1] - points->GetPoint(0)[1], 2) + pow(points->GetPoint(4)[2] - points->GetPoint(0)[2], 2));
  };

  //--------------------------------------------------------------------------------
  void GetQuaterionFromOrientedBBox(vtkPolyData* polyData, vtkPoints* pointsWithoutRotation, double center[3], double length[3], double quat[4])
  {
    double pointWithoutRot[3];
    for (int i = 0; i < 8; ++i) 
    {
      pointWithoutRot[0] = center[0] + length[0] / 2 * (i & 1 ? 1 : -1);
      pointWithoutRot[1] = center[1] + length[1] / 2 * (i & 2 ? 1 : -1);
      pointWithoutRot[2] = center[2] + length[2] / 2 * (i & 4 ? 1 : -1);
      pointsWithoutRotation->InsertNextPoint(pointWithoutRot);
    }

    vtkSmartPointer<vtkLandmarkTransform> landmarkTransform = vtkSmartPointer<vtkLandmarkTransform>::New();
    landmarkTransform->SetSourceLandmarks(pointsWithoutRotation);
    landmarkTransform->SetTargetLandmarks(polyData->GetPoints());
    landmarkTransform->SetModeToRigidBody();
    landmarkTransform->Update();
    
    vtkSmartPointer<vtkMatrix4x4> matrix = landmarkTransform->GetMatrix();
    vtkSmartPointer<vtkTransform> transform = vtkSmartPointer<vtkTransform>::New();
    transform->SetMatrix(matrix);
    transform->GetOrientationWXYZ(quat);
  };
};
