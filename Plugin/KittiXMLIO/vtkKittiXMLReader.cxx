#include "vtkKittiXMLReader.h"
#include "KittiXMLIOUtils.h"

#include <vtkCellArray.h>
#include <vtkDataAssembly.h>
#include <vtkFieldData.h>
#include <vtkFloatArray.h>
#include <vtkIdTypeArray.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkIntArray.h>
#include <vtkMatrix3x3.h>
#include <vtkPartitionedDataSet.h>
#include <vtkPartitionedDataSetCollection.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkPolyLine.h>
#include <vtkSmartPointer.h>
#include <vtkStringArray.h>
#include <vtkTransform.h>
#include <vtkXMLDataElement.h>
#include <vtkXMLDataParser.h>

#include <algorithm>
#include <cctype>
#include <cstring>
#include <iostream>
#include <math.h>
#include <string>

vtkStandardNewMacro(vtkKittiXMLReader);

//----------------------------------------------------------------------------------------------------------------------------------------
vtkKittiXMLReader::vtkKittiXMLReader()
{
    this->FileName = nullptr;
    this->SetNumberOfInputPorts(0);
    this->SetNumberOfOutputPorts(1);
}

//----------------------------------------------------------------------------------------------------------------------------------------
vtkKittiXMLReader::~vtkKittiXMLReader()
{
    delete[] this->FileName;
}

//----------------------------------------------------------------------------------------------------------------------------------------
int vtkKittiXMLReader::RequestData(vtkInformation* vtkNotUsed(request),
                                   vtkInformationVector** vtkNotUsed(inputVector), vtkInformationVector* outputVector)
{
    if (!this->FileName) 
    {
        vtkErrorMacro("A FileName must be specified.");
        return 0;
    }

    vtkInformation* outInfo = outputVector->GetInformationObject(0);
    vtkPartitionedDataSetCollection* outputVTPC = vtkPartitionedDataSetCollection::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

    return this->ReadData(outputVTPC);
}

//----------------------------------------------------------------------------------------------------------------------------------------
int vtkKittiXMLReader::FillOutputPortInformation(int port, vtkInformation* info)
{
    info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkPartitionedDataSetCollection");
    return 1;
}

//----------------------------------------------------------------------------------------------------------------------------------------
int vtkKittiXMLReader::ReadData(vtkPartitionedDataSetCollection* outputVTPC)
{
    KittiXMLIOUtils::KittiXMLLabels labelStruct;

    // Open and read the file
    vtkSmartPointer<vtkXMLDataParser> parser = vtkSmartPointer<vtkXMLDataParser>::New();
    parser->SetFileName(this->FileName);
    parser->Parse();
    vtkSmartPointer<vtkXMLDataElement> root = parser->GetRootElement();
    vtkSmartPointer<vtkXMLDataElement> child = root->GetNestedElement(0);

    if (!child) 
    {
        vtkErrorMacro("No valid data in file");
        return 0;
    }

    // add data assembly for hierarchical data
    vtkSmartPointer<vtkDataAssembly> assembly = vtkSmartPointer<vtkDataAssembly>::New();
    assembly->Initialize();
    outputVTPC->SetDataAssembly(assembly);
    assembly->SetNodeName(0, labelStruct.rootLabel);

    // create lists for the labels:
    vtkSmartPointer<vtkStringArray> labels = vtkSmartPointer<vtkStringArray>::New();
    vtkSmartPointer<vtkIntArray> labelsTable = vtkSmartPointer<vtkIntArray>::New();
    int cptUid = 0;

    for (int uid = 0; uid < child->GetNumberOfNestedElements(); uid++) 
    {
        vtkSmartPointer<vtkPolyData> polydata = vtkSmartPointer<vtkPolyData>::New();
        vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
        vtkSmartPointer<vtkXMLDataElement> item = child->GetNestedElement(uid);

        bool isValidItem = KittiXMLIOUtils::CheckingItemInformation(item);
        if (!isValidItem) 
        {
            continue;
        }

        KittiXMLIOUtils::KittiXMLInfo annotationInfo;
        polydata = KittiXMLIOUtils::CreatePolydataFromItem(item, points, polydata, annotationInfo, labelStruct); // creation of a vtk polydata from one item (annotation)
        
        std::string annotationNameNoSpaces = KittiXMLIOUtils::RemoveSpaces(std::string(item->FindNestedElementWithName(labelStruct.objectTypeLabel)->GetCharacterData()));
        annotationInfo.nameLabel = annotationNameNoSpaces.c_str();

        // check if annotationInfo.nameLabel is not already in the list of labels
        if (labels->LookupValue(annotationInfo.nameLabel) == -1)
        {
            labels->InsertNextValue(annotationInfo.nameLabel);
            // add a childNode to the assembly
            auto labelNode = assembly->AddNode(annotationInfo.nameLabel);
            // add labelNode to labelsTable
            labelsTable->InsertNextValue(labelNode);
        }

        int k = labels->LookupValue(annotationInfo.nameLabel); // get the index of the label in the list of labels (str)
        int element = labelsTable->GetValue(k); // get the index of the node in the labelsTable (int)
        std::string blockName = "Block";
        auto nodeId = assembly->AddNode(blockName.c_str(), element); // add a Node inside the labelNode which contains only one polydata
        std::string blockNameWithNodeId = blockName + std::to_string(nodeId);
        assembly->SetNodeName(nodeId, blockNameWithNodeId.c_str());

        KittiXMLIOUtils::SetFieldDataToPolydata(polydata, annotationInfo.nameLabel, nodeId, annotationInfo, labelStruct); // Set the field data to the polydata
        assembly->AddDataSetIndex(nodeId, outputVTPC->GetNumberOfPartitionedDataSets());
        vtkSmartPointer<vtkPartitionedDataSet> partition = vtkSmartPointer<vtkPartitionedDataSet>::New();
        partition->SetPartition(0, polydata);
        outputVTPC->SetPartitionedDataSet(outputVTPC->GetNumberOfPartitionedDataSets(), partition);

        cptUid++;
    }
    
    return 1;
}

//----------------------------------------------------------------------------------------------------------------------------------------
void vtkKittiXMLReader::PrintSelf(ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);

    os << indent << "File Name: " << (this->FileName ? this->FileName : "(none)") << "\n";
}
