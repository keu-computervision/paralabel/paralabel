/*=========================================================================
  SPDX-FileCopyrightText: Copyright (c) Kitware Inc.
  SPDX-License-Identifier: Apache-2.0
=========================================================================*/
/**
 * @class vtkKittiXMLReader - sample reader
 * .SECTION Description
 * vtkKittiXMLReader is a sample reader imported in ParaView. It reads data from a XML file (format Kitti 1.0) and outputs a vtkPartitionedDataSetCollection.
 * The vtkPartitionedDataSetCollection contains a vtkDataAssembly to explicit the structure of the data and N vtkPartitionedDatasets where N is the number
 * of annotations in the XML file. Each vtkPartitionedDataset contains a vtkPolyData with the annotation data.
*/

#ifndef vtkKittiXMLReader_h
#define vtkKittiXMLReader_h

#include "vtkKittiXMLIOModule.h"
#include <vtkPartitionedDataSetCollectionAlgorithm.h>

class vtkPartitionedDataSetCollection;

class VTKKITTIXMLIO_EXPORT vtkKittiXMLReader : public vtkPartitionedDataSetCollectionAlgorithm
{
public:
  static vtkKittiXMLReader* New();
  vtkTypeMacro(vtkKittiXMLReader, vtkPartitionedDataSetCollectionAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  //@{
  // Description: Specify file name of the Kitti XML file to read.
  vtkSetStringMacro(FileName);
  vtkGetStringMacro(FileName);
  //@}

protected:
  vtkKittiXMLReader();
  ~vtkKittiXMLReader() override;

  int RequestData(vtkInformation*, vtkInformationVector**, vtkInformationVector*);
  int ReadData(vtkPartitionedDataSetCollection* output);
  int FillOutputPortInformation(int, vtkInformation*) override;

private:
  vtkKittiXMLReader(const vtkKittiXMLReader&) = delete;
  void operator=(const vtkKittiXMLReader&) = delete;
  char* FileName = nullptr;
};

#endif
