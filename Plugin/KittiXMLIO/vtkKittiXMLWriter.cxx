#include "vtkKittiXMLWriter.h"
#include "KittiXMLIOUtils.h"

#include <vtkAlgorithm.h>
#include <vtkFieldData.h>
#include <vtkInformation.h>
#include <vtkPartitionedDataSet.h>
#include <vtkPartitionedDataSetCollection.h>
#include <vtkDataAssembly.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkXMLDataElement.h>
#include <vtkXMLDataParser.h>

vtkStandardNewMacro(vtkKittiXMLWriter);

//----------------------------------------------------------------------------
vtkKittiXMLWriter::vtkKittiXMLWriter()
{
    this->FileName = nullptr;
    this->SetNumberOfInputPorts(1);
    this->SetNumberOfOutputPorts(0);
}

//----------------------------------------------------------------------------
vtkKittiXMLWriter::~vtkKittiXMLWriter()
{
    delete[] this->FileName;
}

//----------------------------------------------------------------------------
int vtkKittiXMLWriter::RequestData(vtkInformation* vtkNotUsed(request),
                                   vtkInformationVector** vtkNotUsed(inputVector), vtkInformationVector* vtkNotUsed(outputVector))
{
    if (!this->FileName) 
    {
        vtkErrorMacro("No file name specified");
        return 0;
    }

    this->WriteData();

    return 1;
}

//----------------------------------------------------------------------------
int vtkKittiXMLWriter::FillInputPortInformation(int port, vtkInformation* info)
{
    info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkPartitionedDataSetCollection");
    return 1;
}

//----------------------------------------------------------------------------
vtkPartitionedDataSetCollection* vtkKittiXMLWriter::GetInput()
{
    return vtkPartitionedDataSetCollection::SafeDownCast(this->GetInputDataObject(0, 0));
}

//----------------------------------------------------------------------------
void vtkKittiXMLWriter::WriteData()
{
    /**
     * All the names which are used here to create the XML file are the same as the names in the Kitti 1.0 format XML file.
     */
    KittiXMLIOUtils::KittiXMLLabels labelStruct;
    vtkSmartPointer<vtkPartitionedDataSetCollection> input = this->GetInput();
    if (!input) 
    {
        vtkErrorMacro("No valid input");
        return;
    }
    
    // Open the file
    vtkSmartPointer<vtkXMLDataElement> root = vtkSmartPointer<vtkXMLDataElement>::New(); // creating the root of the XML file
    root->SetName(labelStruct.boostSerializationLabel);
    root->SetAttribute(labelStruct.versionLabel, "9");
    root->SetAttribute(labelStruct.signatureLabel, "serialization::archive");

    vtkSmartPointer<vtkXMLDataElement> child = vtkSmartPointer<vtkXMLDataElement>::New();
    child->SetName(labelStruct.trackletsLabel);
    root->AddNestedElement(child); // child is the child from the root but it is also the parent of all the items (annotations / bounding boxes)

    vtkSmartPointer<vtkXMLDataElement> count = vtkSmartPointer<vtkXMLDataElement>::New();
    count->SetName(labelStruct.countLabel);
    count->SetCharacterData("0", 1);
    child->AddNestedElement(count);

    KittiXMLIOUtils::CreateAndAddXMLElement(labelStruct.itemVersionLabel, "1", child);

    int numPartitionsDatasets = input->GetNumberOfPartitionedDataSets();
    vtkSmartPointer<vtkDataAssembly> assembly = input->GetDataAssembly();
    if (!assembly) 
    {
        vtkErrorMacro("No data assembly");
        return;
    }
    std::vector<unsigned int> datasetIndices = assembly->GetDataSetIndices(0);
    std::sort(datasetIndices.begin(), datasetIndices.end());
    for (int i = 0; i < numPartitionsDatasets; i++) 
    {
        if (std::find(datasetIndices.begin(), datasetIndices.end(), i) == datasetIndices.end())
        {
            continue;
        }

        vtkSmartPointer<vtkPartitionedDataSet> partition = input->GetPartitionedDataSet(i);
        int numPartitions = partition->GetNumberOfPartitions();
        vtkSmartPointer<vtkPolyData> polydata = vtkPolyData::SafeDownCast(partition->GetPartition(0));
        if (!polydata) 
        {
            vtkErrorMacro("Block " << i << " is not a vtkPolyData");
        }
        vtkSmartPointer<vtkXMLDataElement> item = vtkSmartPointer<vtkXMLDataElement>::New();
        item.TakeReference(KittiXMLIOUtils::CreateItemMember(i, polydata, labelStruct)); // creating the member containing the information about the annotation / bounding box
        child->AddNestedElement(item); // adding the information about the annotation / bounding box to the root of the file
    }

    count->SetCharacterData(std::to_string(numPartitionsDatasets).c_str(), std::to_string(numPartitionsDatasets).length());
    root->PrintXML(FileName);
}

void vtkKittiXMLWriter::PrintSelf(ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
    os << indent << "FileName: " << (this->FileName ? this->FileName : "(none)") << endl;
}
