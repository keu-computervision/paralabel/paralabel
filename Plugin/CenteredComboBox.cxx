// CenteredComboBox.cpp
#include "CenteredComboBox.h"
#include <QPainter>
#include <QStylePainter>

void CenteredComboBox::paintEvent(QPaintEvent *event)
{
    QStylePainter painter(this);
    QStyleOptionComboBox opt;
    initStyleOption(&opt);

    painter.drawComplexControl(QStyle::CC_ComboBox, opt);

    // Draw the text centered
    QRect textRect = style()->subControlRect(QStyle::CC_ComboBox, &opt, QStyle::SC_ComboBoxEditField, this);
    painter.drawText(textRect, Qt::AlignCenter, opt.currentText);
}
