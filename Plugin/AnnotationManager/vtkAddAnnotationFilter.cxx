
#include "vtkAddAnnotationFilter.h"

#include "vtkDataAssembly.h"
#include "vtkFieldData.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkPartitionedDataSet.h"
#include "vtkPartitionedDataSetCollection.h"
#include "vtkPolyData.h"
#include "vtkStringArray.h"
#include "vtkTransform.h"

#include "../KittiXMLIO/KittiXMLIOUtils.h"

vtkStandardNewMacro(vtkAddAnnotationFilter);

//----------------------------------------------------------------------------
int vtkAddAnnotationFilter::RequestData(vtkInformation* vtkNotUsed(request),
  vtkInformationVector** inputVector, vtkInformationVector* outputVector)
{
  vtkInformation* inInfo = inputVector[0]->GetInformationObject(0);
  vtkPartitionedDataSetCollection* inPDSC = vtkPartitionedDataSetCollection::GetData(inInfo);
  vtkInformation* outInfo = outputVector->GetInformationObject(0);
  vtkPartitionedDataSetCollection* outPDSC = vtkPartitionedDataSetCollection::GetData(outInfo);

  // Note: Deep copying for every modification seems like a bad idea
  outPDSC->DeepCopy(inPDSC);

	// Instanciate the annotation
	KittiXMLIOUtils::KittiXMLInfo annotationInfo = {
    this->Label.c_str(),
    {
      static_cast<float>(this->Length[0]),
      static_cast<float>(this->Length[1]),
      static_cast<float>(this->Length[2])
    },
    {
      static_cast<float>(this->Position[0]),
      static_cast<float>(this->Position[1]),
      static_cast<float>(this->Position[2])
    },
	  {
      static_cast<float>(this->Rotation[0]),
      static_cast<float>(this->Rotation[1]),
      static_cast<float>(this->Rotation[2])
    },
	  {0.f, 0.f, 0.f, 0.f},
	  {
      static_cast<float>(this->Position[0] - this->Length[0] / 2),
      static_cast<float>(this->Position[0] + this->Length[0] / 2),
      static_cast<float>(this->Position[1] - this->Length[1] / 2),
      static_cast<float>(this->Position[1] + this->Length[1] / 2),
      static_cast<float>(this->Position[2] - this->Length[2] / 2),
      static_cast<float>(this->Position[2] + this->Length[2] / 2)
    },
	  {0.f, 0.f, 0.f}
  };
	// Define the widget points used to create the polydata
  vtkNew<vtkPoints> widgetPoints;
  KittiXMLIOUtils::InsertPoint(widgetPoints, annotationInfo);

  vtkNew<vtkTransform> transform;
  transform->RotateZ(this->Rotation[2]);
  transform->RotateX(this->Rotation[0]);
  transform->RotateY(this->Rotation[1]);

  KittiXMLIOUtils::TransformPoints(widgetPoints, transform, this->Position);

  vtkNew<vtkPolyData> polyData;
  polyData->SetPoints(widgetPoints);

  vtkNew<vtkCellArray> cell;
  cell->InsertNextCell(widgetPoints->GetNumberOfPoints());
  for (int i = 0; i < widgetPoints->GetNumberOfPoints(); ++i)
  {
    cell->InsertCellPoint(i);
  }
  polyData->SetVerts(cell);
  KittiXMLIOUtils::CreateLinesOfCuboid(widgetPoints, polyData);

  vtkDataAssembly* dataAssembly = outPDSC->GetDataAssembly();
  int labelNodeIdx = dataAssembly->FindFirstNodeWithName(this->Label.c_str());
  if (labelNodeIdx == -1)
  {
    labelNodeIdx = dataAssembly->AddNode(this->Label.c_str());
  }

  int blockNodeIdx = dataAssembly->AddNode("Block", labelNodeIdx);
  std::string nodeName = "Block" + std::to_string(blockNodeIdx);
  dataAssembly->SetNodeName(blockNodeIdx, nodeName.c_str());
  dataAssembly->AddDataSetIndex(blockNodeIdx, outPDSC->GetNumberOfPartitionedDataSets());

  // add the polydata to the partitionedDataSetCollection
  vtkNew<vtkPartitionedDataSet> partitionedDataSet;
  partitionedDataSet->SetPartition(0, polyData);
  outPDSC->SetPartitionedDataSet(outPDSC->GetNumberOfPartitionedDataSets(), partitionedDataSet);

  // Add the field arrays
	KittiXMLIOUtils::KittiXMLLabels labelStruct;
  KittiXMLIOUtils::SetFieldDataToPolydata(
    polyData, this->Label.c_str(), blockNodeIdx, annotationInfo, labelStruct);

  return 1;
}

//----------------------------------------------------------------------------
void vtkAddAnnotationFilter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
  os << indent << "Label: " << this->Label << std::endl;
}
