/*=========================================================================
  SPDX-FileCopyrightText: Copyright (c) Kitware Inc.
  SPDX-License-Identifier: Apache-2.0
=========================================================================*/
/**
 * @file TestAnnotationUtils.h
 * @section Description
 * This file contains a namespace with a function to compare two vtkPartitionedDataSetCollection objects.
 */

#ifndef TestAnnotationUtils_h
#define TestAnnotationUtils_h

#include "vtkPartitionedDataSetCollection.h"
#include "vtkPartitionedDataSet.h"
#include "vtkDataAssembly.h"
#include "vtkPolyData.h"
#include "vtkFieldData.h"

namespace TestAnnotationUtils
{
  inline bool comparePDC(vtkPartitionedDataSetCollection* inputPDC, vtkPartitionedDataSetCollection* expectedPDC)
  {
      if (!inputPDC || !expectedPDC)
    {
      std::cerr << "Error reading the output" << std::endl;
      return false;
    }
    if (inputPDC->GetNumberOfPartitionedDataSets() != expectedPDC->GetNumberOfPartitionedDataSets())
    {
      std::cerr << "Number of partitioned datasets is different" << std::endl;
      return false;
    }

    for (int i = 0; i < inputPDC->GetNumberOfPartitionedDataSets(); i++)
    {
      vtkSmartPointer<vtkPartitionedDataSet> inputPD = inputPDC->GetPartitionedDataSet(i);
      vtkSmartPointer<vtkPartitionedDataSet> expectedPD = expectedPDC->GetPartitionedDataSet(i);
      vtkSmartPointer<vtkPolyData> inputPolyData = vtkPolyData::SafeDownCast(inputPD->GetPartition(0));
      vtkSmartPointer<vtkPolyData> expectedPolyData = vtkPolyData::SafeDownCast(expectedPD->GetPartition(0));
      if (!inputPolyData || !expectedPolyData)
      {
        std::cerr << "Error reading the polydata" << std::endl;
        return false;
      }
      vtkSmartPointer<vtkPoints> inputPts = inputPolyData->GetPoints();
      vtkSmartPointer<vtkPoints> expectedPts = expectedPolyData->GetPoints();
      if (inputPts->GetNumberOfPoints() != expectedPts->GetNumberOfPoints())
      {
        std::cerr << "Number of points is different" << std::endl;
        return false;
      }
      for (int j = 0; j < inputPts->GetNumberOfPoints(); j++)
      {
        double inputPoint[3];
        double expectedPoint[3];
        inputPts->GetPoint(j, inputPoint);
        expectedPts->GetPoint(j, expectedPoint);
        for (int k = 0; k < 3; k++)
        {
          if (std::abs(inputPoint[k] - expectedPoint[k]) < 1e-6)
          {
            continue;
          }
          if (inputPoint[k] != expectedPoint[k])
          {
            std::cerr << "Points are different" << std::endl;
            return false;
          }
        }
      }
      if (inputPolyData->GetFieldData()->GetNumberOfArrays() != expectedPolyData->GetFieldData()->GetNumberOfArrays())
      {
        std::cerr << "Number of arrays is different" << std::endl;
        return false;
      }
      for (int f = 0; f < inputPolyData->GetFieldData()->GetNumberOfArrays(); f++)
      {
        vtkSmartPointer<vtkAbstractArray> inputArray = inputPolyData->GetFieldData()->GetAbstractArray(f);
        vtkSmartPointer<vtkAbstractArray> expectedArray = expectedPolyData->GetFieldData()->GetAbstractArray(f);
        if (!inputArray || !expectedArray)
        {
          std::cerr << "Error reading the arrays" << std::endl;
          return false;
        }
        if (inputArray->GetNumberOfTuples() != expectedArray->GetNumberOfTuples())
        {
          std::cerr << "Number of tuples is different" << std::endl;
          return false;
        }

        for (int t = 0; t < inputArray->GetNumberOfTuples(); t++)
        {
          if (inputArray->GetNumberOfComponents() != expectedArray->GetNumberOfComponents())
          {
            std::cerr << "Number of components is different" << std::endl;
            return false;
          }
          for (int c = 0; c < inputArray->GetNumberOfComponents(); c++)
          {
            if (inputArray->GetVariantValue(t) != expectedArray->GetVariantValue(t))
            {
              // print the values to see the difference
              std::cerr << "Values are different" << std::endl;
              return false;
            }
          }
        }
      }
    }

    vtkSmartPointer<vtkDataAssembly> inputDA = vtkSmartPointer<vtkDataAssembly>::New();
    inputDA = inputPDC->GetDataAssembly();
    vtkSmartPointer<vtkDataAssembly> expectedDA = vtkSmartPointer<vtkDataAssembly>::New();
    expectedDA = expectedPDC->GetDataAssembly();
    if (!inputDA || !expectedDA)
    {
      std::cerr << "Error reading the data assembly" << std::endl;
      return false;
    }
    if (inputDA->GetNumberOfChildren(0) != expectedDA->GetNumberOfChildren(0))
    {
      std::cerr << "Number of children is different" << std::endl;
      return false;
    }
    for (int i = 0; i < inputDA->GetNumberOfChildren(0); i++)
    {
      int inputChild = inputDA->GetChild(0, i);
      int expectedChild = expectedDA->GetChild(0, i);
      if (!inputChild || !expectedChild)
      {
        std::cerr << "Error reading the children" << std::endl;
        return false;
      }
      if (inputDA->GetNumberOfChildren(inputChild) != expectedDA->GetNumberOfChildren(expectedChild))
      {
        std::cerr << "Number of children is different" << std::endl;
        return false;
      }
    }
    return true;
  }
};

#endif
