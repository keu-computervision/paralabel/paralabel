#include <filesystem>

#include "vtkPolyData.h"
#include "vtkXMLPolyDataReader.h"
#include "vtkXMLPartitionedDataSetCollectionReader.h"

#include "KittiXMLIOUtils.h"
#include "vtkEditAnnotationFilter.h"
#include "TestAnnotationUtils.h"


int TestEditAnnotation_@test_name@(int argc, char* argv[])
{
  std::filesystem::path testDataDir = "@PATH_ANNOTATION_MANAGER_TESTS@";
  std::filesystem::path inputPDCPath = testDataDir / "Input/test_ahn4.vtpc";
  std::filesystem::path expectedPDCPath = testDataDir / "Expected/test_EditAnnotationManager.vtpc";

  // Read the PDC from input data directory
  vtkNew<vtkXMLPartitionedDataSetCollectionReader> pdcReader;
  pdcReader->SetFileName(inputPDCPath.c_str());
  pdcReader->Update();
  vtkSmartPointer<vtkPartitionedDataSetCollection> inputPDC = vtkPartitionedDataSetCollection::SafeDownCast(pdcReader->GetOutput());

  // Define the edition properties (selected item id and annotation information)
  int selectedAnnotationIdx = 1;
  const char* labelName = "Building";
  double scale3D[3] = {46.f, 60.f, 37.f};
  double position[3] = {119821.f, 483350.5f, 22.5f};
  double rotationXYZ[3] = {0.f, 0.f, 45.f};
  // Edit the annotation item from the input PDC
  vtkNew<vtkEditAnnotationFilter> editAnnotationFilter;
  editAnnotationFilter->SetInputDataObject(inputPDC);
  editAnnotationFilter->SetSelectedAnnotationIdx(selectedAnnotationIdx);
  editAnnotationFilter->SetPosition(position[0], position[1], position[2]);
  editAnnotationFilter->SetRotation(rotationXYZ[0], rotationXYZ[1], rotationXYZ[2]);
  editAnnotationFilter->SetLength(scale3D[0], scale3D[1], scale3D[2]);
  editAnnotationFilter->SetLabel(labelName);
  editAnnotationFilter->Update();
  vtkSmartPointer<vtkPartitionedDataSetCollection> modifiedPDC = vtkPartitionedDataSetCollection::SafeDownCast(editAnnotationFilter->GetOutput());

  // Read the expected PDC and test it against the input PDC with annotation item edited
  vtkNew<vtkXMLPartitionedDataSetCollectionReader> expectedPDCReader;
  expectedPDCReader->SetFileName(expectedPDCPath.c_str());
  expectedPDCReader->Update();
  vtkSmartPointer<vtkPartitionedDataSetCollection> expectedPDC = vtkPartitionedDataSetCollection::SafeDownCast(expectedPDCReader->GetOutput());
  bool result = TestAnnotationUtils::comparePDC(modifiedPDC, expectedPDC);
  if (!result)
  {
    std::cerr << "Error comparing the output" << std::endl;
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
