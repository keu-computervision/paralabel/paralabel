// SPDX-FileCopyrightText: Copyright (c) Kitware Inc.
// SPDX-FileCopyrightText: Copyright (c) Sandia Corporation
// SPDX-License-Identifier: BSD-3-Clause
/**
 * @class vtkGetSelection
 * @brief This class is a filter which will return the string path of a selected block from a vtkPartitionedDataSetCollection
*/

#ifndef vtkGetSelection_h
#define vtkGetSelection_h

#include "vtkFiltersGeneralModule.h" // For export macro
#include "vtkDataObjectAlgorithm.h"
#include "vtkStringArray.h"
#include "vtkSmartPointer.h"

class VTKFILTERSGENERAL_EXPORT vtkGetSelection : public vtkDataObjectAlgorithm
{
public:
    static vtkGetSelection* New();
    vtkTypeMacro(vtkGetSelection, vtkDataObjectAlgorithm);
    void PrintSelf(ostream& os, vtkIndent indent) override;

    void SetSource(vtkAlgorithmOutput* annotationInput);

    /**
     * @brief GetSelectionOutput
     * Get the output of the filter
    */
    vtkStringArray* GetSelectionOutput();

protected:
    vtkGetSelection();
    ~vtkGetSelection() override = default;
    int RequestData(vtkInformation* request, vtkInformationVector** inputVector, vtkInformationVector* outputVector) override;
    int FillInputPortInformation(int port, vtkInformation* info) override;
    int FillOutputPortInformation(int port, vtkInformation* info) override;

private:
    vtkSmartPointer<vtkStringArray> SelectionOutput = vtkSmartPointer<vtkStringArray>::New();
    vtkGetSelection(const vtkGetSelection&) = delete;
    void operator=(const vtkGetSelection&) = delete;
};

#endif
