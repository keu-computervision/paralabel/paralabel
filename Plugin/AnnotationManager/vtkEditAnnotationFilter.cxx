#include "vtkEditAnnotationFilter.h"

#include "vtkDataAssembly.h"
#include "vtkFieldData.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkPartitionedDataSet.h"
#include "vtkPartitionedDataSetCollection.h"
#include "vtkPolyData.h"
#include "vtkStringArray.h"
#include "vtkTransform.h"

#include "../KittiXMLIO/KittiXMLIOUtils.h"

vtkStandardNewMacro(vtkEditAnnotationFilter);

//----------------------------------------------------------------------------
int vtkEditAnnotationFilter::RequestData(vtkInformation* vtkNotUsed(request),
  vtkInformationVector** inputVector, vtkInformationVector* outputVector)
{
  KittiXMLIOUtils::KittiXMLInfo widgetInfo;
  widgetInfo.bounds[0] = this->Position[0] - this->Length[0] / 2;
  widgetInfo.bounds[1] = this->Position[0] + this->Length[0] / 2;
  widgetInfo.bounds[2] = this->Position[1] - this->Length[1] / 2;
  widgetInfo.bounds[3] = this->Position[1] + this->Length[1] / 2;
  widgetInfo.bounds[4] = this->Position[2] - this->Length[2] / 2;
  widgetInfo.bounds[5] = this->Position[2] + this->Length[2] / 2;

  vtkNew<vtkPoints> widgetPoints;
  KittiXMLIOUtils::InsertPoint(widgetPoints, widgetInfo);

  if (this->Rotation[0] != 0 || this->Rotation[1] != 0 || this->Rotation[2] != 0)
  {
    vtkNew<vtkTransform> transform;
    transform->RotateZ(this->Rotation[2]);
    transform->RotateX(this->Rotation[0]);
    transform->RotateY(this->Rotation[1]);
    KittiXMLIOUtils::TransformPoints(widgetPoints, transform, this->Position);
  }

  vtkInformation* inInfo = inputVector[0]->GetInformationObject(0);
  vtkPartitionedDataSetCollection* inPDSC =
    vtkPartitionedDataSetCollection::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));

  vtkInformation* outInfo = outputVector->GetInformationObject(0);
  vtkPartitionedDataSetCollection* outPDSC =
    vtkPartitionedDataSetCollection::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

  outPDSC->DeepCopy(inPDSC);

  vtkPartitionedDataSet* selectedPDS = outPDSC->GetPartitionedDataSet(this->SelectedAnnotationIdx);
  vtkPolyData* selectedPD = vtkPolyData::SafeDownCast(selectedPDS->GetPartitionAsDataObject(0));

  selectedPD->SetPoints(widgetPoints);

  KittiXMLIOUtils::KittiXMLInfo annotationInfo;
  annotationInfo.nameLabel = this->Label.c_str();

  KittiXMLIOUtils::GetValuesOfPolydata(selectedPD, annotationInfo);

  vtkFieldData* selectedFD = selectedPD->GetFieldData();

  vtkStringArray* labelArray = vtkStringArray::SafeDownCast(selectedFD->GetAbstractArray("label"));
  std::string labelValue = labelArray->GetValue(0);
  KittiXMLIOUtils::KittiXMLLabels labelStruct;
  KittiXMLIOUtils::ModifyFieldData(selectedFD, annotationInfo, labelStruct);

  vtkIdTypeArray* uidArray = vtkIdTypeArray::SafeDownCast(selectedFD->GetArray("uid"));
  int nodeId = uidArray->GetValue(0);
  if (labelValue != this->Label)
  {
    vtkDataAssembly* dataAssembly = outPDSC->GetDataAssembly();
    dataAssembly->RemoveNode(nodeId);

    // now check if there is already a node with Label this->label
    int labelToNode = dataAssembly->FindFirstNodeWithName(this->Label.c_str());
    if (labelToNode == -1)
    {
      int newLabelNode = dataAssembly->AddNode(this->Label.c_str());
      std::string nodeName = "Block";
      int newNode = dataAssembly->AddNode(nodeName.c_str(), newLabelNode);
      std::string nodeNameUId = nodeName + std::to_string(newNode);
      dataAssembly->SetNodeName(newNode, nodeNameUId.c_str());

      // set newnode value to uidarray
      uidArray->SetValue(0, newNode);
      dataAssembly->AddDataSetIndex(newNode, this->SelectedAnnotationIdx);
    }
  }

  return 1;
}

//----------------------------------------------------------------------------
void vtkEditAnnotationFilter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
  os << indent << "Label: " << this->Label << std::endl;
}
