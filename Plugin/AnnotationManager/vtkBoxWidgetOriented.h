// SPDX-FileCopyrightText: Copyright (c) Kitware Inc.
// SPDX-FileCopyrightText: Copyright (c) Sandia Corporation
// SPDX-License-Identifier: BSD-3-Clause
/**
 * @class vtkBoxWidgetOriented
 * @brief This class is used to create a specialized box widget that can be oriented and placed based on points for oriented boxes
*/

#ifndef vtkBoxWidgetOriented_h
#define vtkBoxWidgetOriented_h

#include "vtkBoxWidget.h"
#include "vtkPoints.h"

class vtkBoxWidgetOriented : public vtkBoxWidget
{
public:
  static vtkBoxWidgetOriented* New();
  vtkTypeMacro(vtkBoxWidgetOriented, vtkBoxWidget);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  /**
   * @brief PlaceWidget
   * @Description: Place the widget based on the points, needed if there is a box oriented
  */
  void PlaceWidget(vtkPoints* points);

  /**
   * @brief PlaceWidget
   * @Description: Place the widget based on the bounds
  */
  void PlaceWidget(double bounds[6]) override;

  static void ProcessEvents(
    vtkObject* object, unsigned long event, void* clientdata, void* calldata);

  static unsigned long UpEvent;
  static unsigned long WidgetModifiedEvent;
  
protected:
  vtkBoxWidgetOriented();
  ~vtkBoxWidgetOriented() = default;

private:
  vtkBoxWidgetOriented(const vtkBoxWidgetOriented&) = delete;
  void operator=(const vtkBoxWidgetOriented&) = delete;
};

#endif
