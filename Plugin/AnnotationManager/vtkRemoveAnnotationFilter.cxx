#include "vtkRemoveAnnotationFilter.h"

#include "vtkObjectFactory.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkPartitionedDataSetCollection.h"
#include "vtkDataAssembly.h"
#include "vtkSmartPointer.h"
#include "vtkStringArray.h"

vtkStandardNewMacro(vtkRemoveAnnotationFilter);

//----------------------------------------------------------------------------
int vtkRemoveAnnotationFilter::RequestData(vtkInformation* vtkNotUsed(request),
  vtkInformationVector** inputVector, vtkInformationVector* outputVector)
{
  vtkInformation* inInfo = inputVector[0]->GetInformationObject(0);
  vtkPartitionedDataSetCollection* input = vtkPartitionedDataSetCollection::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));

  if (!input)
  {
    vtkErrorMacro("No input data");
    return 0;
  }

  vtkInformation* outInfo = outputVector->GetInformationObject(0);
  vtkPartitionedDataSetCollection* output = vtkPartitionedDataSetCollection::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));

  output->DeepCopy(input);

  vtkSmartPointer<vtkDataAssembly> outputAssembly = output->GetDataAssembly();
  if (!outputAssembly)
  {
    vtkErrorMacro("No output assembly");
    return 0;
  }

	if (this->Selections->GetNumberOfValues() == 0)
	{
		vtkErrorMacro("No selection");
    return 0;
	}
  for (int i = 0; i < this->Selections->GetNumberOfValues(); i++)
  {
    const char* name = this->Selections->GetValue(i).c_str();
    size_t pos = std::string(name).find_last_of("/");
    std::string lastName = std::string(name).substr(pos+1);
    int nodeIdx = outputAssembly->FindFirstNodeWithName(lastName.c_str());
    if (nodeIdx == -1)
    {
      vtkErrorMacro("Node with name not found");
      return 0;
    }
    outputAssembly->RemoveNode(nodeIdx);
  }

  return 1;
}

//----------------------------------------------------------------------------
void vtkRemoveAnnotationFilter::AddSelection(std::string selection)
{
	vtkDebugMacro(<< " setting selection to " << selection);
  this->Selections->InsertNextValue(selection);
	this->Modified();
}

//----------------------------------------------------------------------------
void vtkRemoveAnnotationFilter::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
