// SPDX-FileCopyrightText: Copyright (c) Kitware Inc.
// SPDX-FileCopyrightText: Copyright (c) Sandia Corporation
// SPDX-License-Identifier: BSD-3-Clause
/**
 * @class vtkRemoveAnnotationFilter
 * @brief This class is used to remove a partitioned dataset or a group of partitioned datasets from a vtkPartitionedDataSetCollection
*/

#ifndef vtkRemoveAnnotationFilter_h
#define vtkRemoveAnnotationFilter_h

#include "vtkFiltersGeneralModule.h" // For export macro
#include "vtkStringArray.h"
#include "vtkSmartPointer.h"
#include <vtkPartitionedDataSetCollectionAlgorithm.h>

class VTKFILTERSGENERAL_EXPORT vtkRemoveAnnotationFilter : public vtkPartitionedDataSetCollectionAlgorithm
{
public:
  static vtkRemoveAnnotationFilter* New();
  vtkTypeMacro(vtkRemoveAnnotationFilter, vtkPartitionedDataSetCollectionAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  /**
   * @brief vtkRemoveAnnotationFilter::AddSelection
   * Add the selection (a data assembly block path) to the input list
  */
  void AddSelection(std::string selection);
  
protected:
  vtkRemoveAnnotationFilter() = default;
  ~vtkRemoveAnnotationFilter() = default;
  int RequestData(vtkInformation* request, vtkInformationVector** inputVector, vtkInformationVector* outputVector) override;
  
private:
  vtkNew<vtkStringArray> Selections;

  vtkRemoveAnnotationFilter(const vtkRemoveAnnotationFilter&) = delete;
  void operator=(const vtkRemoveAnnotationFilter&) = delete;
};

#endif
