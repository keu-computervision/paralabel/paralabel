#include "vtkGetSelection.h"

#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkSelection.h"
#include "vtkSelectionNode.h"
#include "vtkPartitionedDataSetCollection.h"
#include "vtkDataSetAttributes.h"
#include "vtkStringArray.h"
#include "vtkDataObject.h"

vtkStandardNewMacro(vtkGetSelection);

//----------------------------------------------------------------------------
vtkGetSelection::vtkGetSelection()
{
    this->SetNumberOfInputPorts(2);
}

//----------------------------------------------------------------------------
int vtkGetSelection::FillInputPortInformation(int port, vtkInformation* info)
{
    if (port == 0)
    {
        info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkSelection");
        return 1;
    }
    else if (port == 1)
    {
        info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkPartitionedDataSetCollection");
        return 1;
    }
    return 0;
}

//----------------------------------------------------------------------------
int vtkGetSelection::FillOutputPortInformation(int vtkNotUsed(port), vtkInformation* info)
{
    info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkDataObject");
    return 1;
}

//----------------------------------------------------------------------------
void vtkGetSelection::SetSource(vtkAlgorithmOutput* annotationInput)
{
    this->SetInputConnection(1, annotationInput);
}

//----------------------------------------------------------------------------
vtkStringArray* vtkGetSelection::GetSelectionOutput()
{
    return this->SelectionOutput;
}

//----------------------------------------------------------------------------
int vtkGetSelection::RequestData(vtkInformation* vtkNotUsed(request),
    vtkInformationVector** inputVector, vtkInformationVector* outputVector)
{
    vtkInformation* inInfo = inputVector[0]->GetInformationObject(0);
    vtkSelection* input = vtkSelection::SafeDownCast(inInfo->Get(vtkDataObject::DATA_OBJECT()));
    vtkInformation* PDCInfo = inputVector[1]->GetInformationObject(0);
    vtkPartitionedDataSetCollection* PDC = vtkPartitionedDataSetCollection::SafeDownCast(PDCInfo->Get(vtkDataObject::DATA_OBJECT()));

    vtkInformation* outInfo = outputVector->GetInformationObject(0);
    vtkDataObject* output = vtkDataObject::SafeDownCast(outInfo->Get(vtkDataObject::DATA_OBJECT()));
    vtkSelectionNode* nodeSelected = input->GetNode(0);
    vtkAbstractArray* selectionArray = nodeSelected->GetSelectionList();
    vtkStringArray* selectionStringArray = vtkStringArray::SafeDownCast(selectionArray);
    selectionStringArray->SetName("Selection");
    output->GetFieldData()->AddArray(selectionStringArray);

    return 1;
}

//----------------------------------------------------------------------------
void vtkGetSelection::PrintSelf(ostream& os, vtkIndent indent)
{
    this->Superclass::PrintSelf(os, indent);
}
