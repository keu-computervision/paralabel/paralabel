#include "vtkBoxWidgetOriented.h"

#include "vtkCallbackCommand.h"
#include "vtkCommand.h"
#include "vtkMath.h"
#include "vtkObjectFactory.h"
#include "vtkPoints.h"
#include "vtkBoxWidget.h"

unsigned long vtkBoxWidgetOriented::UpEvent = vtkCommand::UserEvent + 2;
unsigned long vtkBoxWidgetOriented::WidgetModifiedEvent = vtkCommand::UserEvent + 3;

vtkStandardNewMacro(vtkBoxWidgetOriented);

//----------------------------------------------------------------------------
vtkBoxWidgetOriented::vtkBoxWidgetOriented()
{
  this->Superclass::EventCallbackCommand->SetCallback(vtkBoxWidgetOriented::ProcessEvents);
}

//----------------------------------------------------------------------------
void vtkBoxWidgetOriented::PlaceWidget(vtkPoints* pts)
{
  for (int i = 0; i < 8; i++)
  {
    this->Points->SetPoint(i, pts->GetPoint(i));
  }

  for (int i = 0; i < 3; ++i)
  {
    std::vector<double> values;
    for (int j = 0; j < 8; ++j)
    {
      values.push_back(pts->GetPoint(j)[i]);
    }
    this->InitialBounds[i * 2] = *std::min_element(values.begin(), values.end());
    this->InitialBounds[i * 2 + 1] = *std::max_element(values.begin(), values.end());
  }
  double dx = this->InitialBounds[1] - this->InitialBounds[0];
  double dy = this->InitialBounds[3] - this->InitialBounds[2];
  double dz = this->InitialBounds[5] - this->InitialBounds[4];
  this->InitialLength = sqrt(dx * dx + dy * dy + dz * dz);

  this->PositionHandles();
  this->ComputeNormals();
  this->SizeHandles();
}

//----------------------------------------------------------------------------
void vtkBoxWidgetOriented::ProcessEvents(
  vtkObject* vtkNotUsed(object), unsigned long event, void* clientdata, void* vtkNotUsed(calldata))
{
  vtkBoxWidgetOriented* self = reinterpret_cast<vtkBoxWidgetOriented*>(clientdata);

  switch (event)
  {
    case vtkCommand::LeftButtonPressEvent:
      self->OnLeftButtonDown();
      break;
    case vtkCommand::LeftButtonReleaseEvent:
      self->OnLeftButtonUp();
      self->InvokeEvent(vtkBoxWidgetOriented::UpEvent, nullptr);
      break;
    case vtkCommand::MiddleButtonPressEvent:
      self->OnMiddleButtonDown();
      break;
    case vtkCommand::MiddleButtonReleaseEvent:
      self->OnMiddleButtonUp();
      self->InvokeEvent(vtkBoxWidgetOriented::UpEvent, nullptr);
      break;
    case vtkCommand::RightButtonPressEvent:
      self->OnRightButtonDown();
      break;
    case vtkCommand::RightButtonReleaseEvent:
      self->OnRightButtonUp();
      self->InvokeEvent(vtkBoxWidgetOriented::UpEvent, nullptr);
      break;
    case vtkCommand::MouseMoveEvent:
      self->OnMouseMove();
      break;
  }

  if (self->State == vtkBoxWidget::Moving || self->State == vtkBoxWidget::Scaling)
  {
    self->InvokeEvent(vtkBoxWidgetOriented::WidgetModifiedEvent);
  }
}

//----------------------------------------------------------------------------
void vtkBoxWidgetOriented::PlaceWidget(double bounds[6])
{
  this->Superclass::PlaceWidget(bounds);
}

//----------------------------------------------------------------------------
void vtkBoxWidgetOriented::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}
