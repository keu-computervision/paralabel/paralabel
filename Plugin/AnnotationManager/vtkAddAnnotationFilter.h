
#ifndef vtkAddAnnotationFilter_h
#define vtkAddAnnotationFilter_h

#include "vtkDataObjectAlgorithm.h"
#include "vtkFiltersGeneralModule.h" // For export macro
#include "vtkPartitionedDataSetCollection.h"
#include <vtkPartitionedDataSetCollectionAlgorithm.h>
#include <vtkVector.h>

class VTKFILTERSGENERAL_EXPORT vtkAddAnnotationFilter : public vtkPartitionedDataSetCollectionAlgorithm
{
public:
  static vtkAddAnnotationFilter* New();
  vtkTypeMacro(vtkAddAnnotationFilter, vtkPartitionedDataSetCollectionAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  vtkSetVector3Macro(Position, double);
  vtkGetVector3Macro(Position, double);

  vtkSetVector3Macro(Rotation, double);
  vtkGetVector3Macro(Rotation, double);

  vtkSetVector3Macro(Length, double);
  vtkGetVector3Macro(Length, double);

  vtkSetMacro(Label, std::string);
  vtkGetMacro(Label, std::string);

protected:
  vtkAddAnnotationFilter() = default;
  ~vtkAddAnnotationFilter() override = default;

  int RequestData(vtkInformation* request, vtkInformationVector** inputVector,
    vtkInformationVector* outputVector) override;

private:
  double Position[3];
  double Rotation[3];
  double Length[3];

  std::string Label = "";

  vtkAddAnnotationFilter(const vtkAddAnnotationFilter&) = delete;
  void operator=(const vtkAddAnnotationFilter&) = delete;
};

#endif
