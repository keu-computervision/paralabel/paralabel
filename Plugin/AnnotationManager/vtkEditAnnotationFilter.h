#ifndef vtkEditAnnotationFilter_h
#define vtkEditAnnotationFilter_h

#include "vtkDataObjectAlgorithm.h"
#include "vtkFiltersGeneralModule.h" // For export macro
#include "vtkPartitionedDataSetCollectionAlgorithm.h"
#include <vtkVector.h>

class VTKFILTERSGENERAL_EXPORT vtkEditAnnotationFilter : public vtkPartitionedDataSetCollectionAlgorithm
{
public:
  static vtkEditAnnotationFilter* New();
  vtkTypeMacro(vtkEditAnnotationFilter, vtkPartitionedDataSetCollectionAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent) override;

  vtkSetVector3Macro(Position, double);
  vtkGetVector3Macro(Position, double);

  vtkSetVector3Macro(Rotation, double);
  vtkGetVector3Macro(Rotation, double);

  vtkSetVector3Macro(Length, double);
  vtkGetVector3Macro(Length, double);

  vtkSetMacro(SelectedAnnotationIdx, int);
  vtkGetMacro(SelectedAnnotationIdx, int);

  vtkSetMacro(Label, std::string);
  vtkGetMacro(Label, std::string);

protected:
  vtkEditAnnotationFilter() = default;
  ~vtkEditAnnotationFilter() override = default;

  int RequestData(vtkInformation* request, vtkInformationVector** inputVector,
    vtkInformationVector* outputVector) override;

private:
  double Position[3];
  double Rotation[3];
  double Length[3];
  std::string Label = "";

  int SelectedAnnotationIdx = 0;

  vtkEditAnnotationFilter(const vtkEditAnnotationFilter&) = delete;
  void operator=(const vtkEditAnnotationFilter&) = delete;
};

#endif
