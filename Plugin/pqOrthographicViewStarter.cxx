// SPDX-FileCopyrightText: Copyright (c) Kitware Inc.
// SPDX-FileCopyrightText: Copyright (c) Sandia Corporation
// SPDX-License-Identifier: BSD-3-Clause
#include "pqOrthographicViewStarter.h"

#include <pqAnimationManager.h>
#include <pqAnimationScene.h>
#include <pqPVApplicationCore.h>
#include <pqActiveObjects.h>
#include <pqObjectBuilder.h>
#include <pqRenderView.h>
#include <pqView.h>
#include <pqServer.h>
#include <pqScalarBarRepresentation.h>

#include <vtkLogger.h>
#include <vtkSMViewLayoutProxy.h>
#include <vtkSMRenderViewProxy.h>
#include <vtkSMPropertyHelper.h>
#include <pqServerManagerModel.h>
#include <pqDataRepresentation.h>
#include <pqPipelineSource.h>
#include <vtkSMColorMapEditorHelper.h>
#include <vtkSMPVRepresentationProxy.h>
#include <vtkSMProxyLink.h>
#include <vtkSMRepresentationProxy.h>
#include <vtkPVDataInformation.h>
#include <vtkSmartPointer.h>
#include <vtkSMViewLink.h>

#include "vtkRenderWindowInteractor.h"
#include "vtkInteractorStyle.h"

#include <QTimer>

//-----------------------------------------------------------------------------
void pqOrthographicViewStarter::onStartup()
{
  pqView* mainView = pqActiveObjects::instance().activeView();
  
  if (!mainView)
  {
    return;
  }

  //create a specific layout for the orthographic views
  vtkSMViewLayoutProxy* layout = pqActiveObjects::instance().activeLayout();
  layout->SplitVertical(0,0.5);
  layout->SplitHorizontal(2,0.5);
  layout->SplitHorizontal(6,0.5);
  layout->EqualizeViews();

  pqServerManagerModel* pqModelInstance = pqPVApplicationCore::instance()->getServerManagerModel();
  pqServer* pqServerInstance = pqActiveObjects::instance().activeServer();
  pqObjectBuilder* pqBuilderInstance = pqPVApplicationCore::instance()->getObjectBuilder();
  if (!pqModelInstance || !pqServerInstance || !pqBuilderInstance)
  {
    return;
  }

  //create the 3 orthographic render views for Front, Top and Side views
  std::vector<pqRenderView*> orthographicRenderViews(3);

  for (int viewIdx = 0; viewIdx < orthographicRenderViews.size(); ++viewIdx) 
  {
      orthographicRenderViews[viewIdx] = qobject_cast<pqRenderView*>(pqBuilderInstance->createView(pqRenderView::renderViewType(), pqServerInstance));
      orthographicRenderViews[viewIdx]->rename(this->viewNames[viewIdx+1]);
      layout->AssignView(this->layoutIdx[viewIdx], orthographicRenderViews[viewIdx]->getViewProxy());
      vtkSMPropertyHelper(orthographicRenderViews[viewIdx]->getViewProxy(), "InteractionMode").Set(1);  // set interaction mode to 1 (2D)
      if (this->viewDirections[viewIdx] == 1) {
          orthographicRenderViews[viewIdx]->resetViewDirectionToPositiveX();
      } else if (this->viewDirections[viewIdx] == 2) {
          orthographicRenderViews[viewIdx]->resetViewDirectionToPositiveY();
      } else if (this->viewDirections[viewIdx] == -3) {
          orthographicRenderViews[viewIdx]->resetViewDirectionToNegativeZ();
      }
  }

  pqActiveObjects::instance().setActiveView(mainView); // because the active view is the last view created
  mainView->rename(this->viewNames[0]);

  QObject::connect(pqModelInstance, &pqServerManagerModel::sourceAdded, this, &pqOrthographicViewStarter::onSourceAdded);
  QObject::connect(mainView, &pqView::representationAdded, this, &pqOrthographicViewStarter::onRepresentationAdded);

  pqRenderView* mainRenderView = qobject_cast<pqRenderView*>(mainView);
  vtkSmartPointer<vtkRenderWindowInteractor> interactor = mainRenderView->getRenderViewProxy()->GetInteractor();
  vtkInteractorStyle* style = vtkInteractorStyle::SafeDownCast(interactor->GetInteractorStyle());

}

//-----------------------------------------------------------------------------
void pqOrthographicViewStarter::setActiveViews()  //set activeViews
{
  auto views = pqPVApplicationCore::instance()->getServerManagerModel()->findItems<pqView*>(pqActiveObjects::instance().activeServer());
  for (auto& view : views)
  {
    view->resetDisplay(true);
  }
}

//-----------------------------------------------------------------------------
void pqOrthographicViewStarter::onSourceAdded(pqPipelineSource* source)
{  
  auto views = pqPVApplicationCore::instance()->getServerManagerModel()->findItems<pqView*>(pqActiveObjects::instance().activeServer());
  if (views.size() > 4)     // check if we still are in the RenderWindow containing the 4 othographic views
  {
    QString viewName = pqActiveObjects::instance().activeView()->getSMName();
    for (int i = 0; i < 4; i++) 
    {
      if (viewName != this->viewNames[i]) 
      {
          return;
      }
    }
  }
  
  if (pqActiveObjects::instance().activeView()->getSMName()!= this->viewNames[0])
  {
    for (int viewIdx = 0; viewIdx < views.size(); ++viewIdx)
    {
      if (views[viewIdx]->getSMName() == this->viewNames[0])
      {
        pqActiveObjects::instance().setActiveView(views[viewIdx]);    // force the main view to be the active view to create the orthographic representations
      }
    }
  }
}

//-----------------------------------------------------------------------------
void pqOrthographicViewStarter::onColorTransferFunctionModified()
{
  pqServerManagerModel* pqModelInstance = pqPVApplicationCore::instance()->getServerManagerModel();
  pqServer* pqServerInstance = pqActiveObjects::instance().activeServer();

  auto scalarBars = pqModelInstance->findItems<pqScalarBarRepresentation*>(pqServerInstance);
  auto sources = pqModelInstance->findItems<pqPipelineSource*>(pqServerInstance);

  if ((sources.size() > 1) && scalarBars.size() > 0)
  {
    for (int barIdx = 0; barIdx < scalarBars.size(); ++barIdx)
    {
      scalarBars[barIdx]->setVisible(true);
    }
  } 
  
  setActiveViews();
}

//-----------------------------------------------------------------------------
void pqOrthographicViewStarter::createReciprocalViewLink(vtkSMProxy* view1Proxy, vtkSMProxy* view2Proxy)
{
  vtkSmartPointer<vtkSMViewLink> viewLink = vtkSmartPointer<vtkSMViewLink>::New();
  viewLink->AddLinkedProxy(view1Proxy, 1);
  viewLink->AddLinkedProxy(view2Proxy, 2);
  viewLink->AddLinkedProxy(view2Proxy, 1);
  viewLink->AddLinkedProxy(view1Proxy, 2);
  viewLink->EnableCameraLink(false);    // disable the camera link
  this->viewLinkStorage.push_back(viewLink);

  vtkSmartPointer<vtkSMProxyLink> axesGridLink = vtkSmartPointer<vtkSMProxyLink>::New();
  axesGridLink->LinkProxyPropertyProxies(view1Proxy, view2Proxy, "AxesGrid");
  this->linkStorage.push_back(axesGridLink);
}

//-----------------------------------------------------------------------------
void pqOrthographicViewStarter::createDataRepresentation(pqDataRepresentation* dataRepresentation, pqPipelineSource* source)
{
  pqObjectBuilder* pqBuilderInstance = pqPVApplicationCore::instance()->getObjectBuilder();

  auto views = pqPVApplicationCore::instance()->getServerManagerModel()->findItems<pqView*>(pqActiveObjects::instance().activeServer());
  std::vector<pqDataRepresentation*> dataRepresentations(views.size());
  std::vector<vtkSmartPointer<vtkSMProxyLink>> links(views.size());
  QObject::connect(dataRepresentation, &pqDataRepresentation::colorTransferFunctionModified, this, &pqOrthographicViewStarter::onColorTransferFunctionModified);
  
  pqView* activeView = dataRepresentation->getView();
  for (int viewIdx = 0; viewIdx < views.size(); ++viewIdx) 
  {
    if (views[viewIdx] == activeView)
    {
      dataRepresentations[viewIdx] = dataRepresentation;
      continue;
    }
    //create a data representation for each view
    dataRepresentations[viewIdx] = pqBuilderInstance->createDataRepresentation(source->getOutputPort(0), views[viewIdx]);
    dataRepresentations[viewIdx]->setVisible(true);
    QObject::connect(dataRepresentations[viewIdx], &pqDataRepresentation::colorTransferFunctionModified, this, &pqOrthographicViewStarter::onColorTransferFunctionModified);

    // Link the data representations
    links[viewIdx] = vtkSmartPointer<vtkSMProxyLink>::New();
    links[viewIdx]->LinkProxies(dataRepresentation->getProxy(), dataRepresentations[viewIdx]->getProxy());
    this->linkStorage.push_back(links[viewIdx]);

    vtkSmartPointer<vtkSMProxyLink> dataAxesGridLink = vtkSmartPointer<vtkSMProxyLink>::New();
    dataAxesGridLink->LinkProxyPropertyProxies(dataRepresentation->getProxy(), dataRepresentations[viewIdx]->getProxy(), "DataAxesGrid");
    this->linkStorage.push_back(dataAxesGridLink);
  } 
}

//-----------------------------------------------------------------------------
void pqOrthographicViewStarter::onRepresentationAdded(pqRepresentation* repAdded)
{
  vtkSMPVRepresentationProxy* repProxy = vtkSMPVRepresentationProxy::SafeDownCast(repAdded->getProxy());

  /**
   * check if the representation is a data representation of a source or a colormap
   * if it is a data representation, create a data representation for each view and link them to the representation added
   * if it is a colormap, do nothing, we just need one data representation of a colormap for the main 3D View
  */
  if (repProxy)
  {
    pqDataRepresentation* dataRepresentation = qobject_cast<pqDataRepresentation*>(repAdded);
    pqPipelineSource* source = dataRepresentation->getInput();

    if (dataRepresentation && source)
    {
      this->createDataRepresentation(dataRepresentation, source);
    }

    bool surface = vtkSMRepresentationProxy::SetRepresentationType(dataRepresentation->getProxy(), "Surface");
    if (!surface)
    {
      vtkLog(WARNING, "Message from pqOrthographicViewStarter: Surface representation not available");
    }
    
    if (source->getSourceProxy()->GetDataInformation()->GetArrayInformation("label", vtkDataObject::FIELD))   //check if the source has a label field data
    {
      vtkSMColorMapEditorHelper::SetScalarColoring(dataRepresentation->getProxy(), "label", vtkDataObject::FIELD);
    }
    else
    {
      vtkLog(WARNING, "Message from pqOrthographicViewStarter: No label field data");
    }
  } 
}
