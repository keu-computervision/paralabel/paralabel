set(tests_xml
  TestOrthographicView.xml
)

set(TestOrthographicView_USES_DIRECT_DATA ON)

# Download test data for paraview XML UI tests
include(ExternalData)
set(ExternalData_URL_TEMPLATES "https://data.kitware.com/api/v1/file/hashsum/%(algo)/%(hash)/download")
set(_paraview_add_tests_default_test_data_target ParaViewData)
ExternalData_Expand_Arguments(ParaViewData _ 
  DATA{../Data/Input/test_ahn4.xml}
  DATA{../Data/Baseline/main_view.png}
  DATA{../Data/Baseline/top_view.png}
  DATA{../Data/Baseline/side_view.png}
  DATA{../Data/Baseline/front_view.png}
)
ExternalData_Add_Target(ParaViewData SHOW_PROGRESS ON)

paraview_add_client_tests(
  DATA_DIRECTORY "${CMAKE_BINARY_DIR}/Plugin/Testing/Data"
  TEST_DATA_TARGET ParaViewData
  TEST_SCRIPTS ${tests_xml}
  LOAD_PLUGIN Paralabel
  PLUGIN_PATH $<TARGET_FILE:Paralabel>
)
