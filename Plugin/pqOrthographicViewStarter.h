// SPDX-FileCopyrightText: Copyright (c) Kitware Inc.
// SPDX-FileCopyrightText: Copyright (c) Sandia Corporation
// SPDX-License-Identifier: BSD-3-Clause
/**
 * @class pqOrthographicViewStarter
 * @brief This class is used when the application starts and create a layout with 4 views (orthographic views) and add representations to the views
*/

#ifndef pqOrthographicViewStarter_h
#define pqOrthographicViewStarter_h

#include <QObject>
#include <QMouseEvent>
#include <vector>
#include <vtkSmartPointer.h>
#include <vtkSMProxyLink.h>
#include <vtkSMViewLink.h>

#include "pqDataRepresentation.h"
#include "pqPipelineSource.h"
#include "pqRenderView.h"
#include "pqRepresentation.h"

class vtkBoxWidget;

class pqOrthographicViewStarter : public QObject
{
  Q_OBJECT
  typedef QObject Superclass;

public:
  pqOrthographicViewStarter(QObject* p = nullptr) : QObject(p) {}
  ~pqOrthographicViewStarter() = default;

public Q_SLOTS:
  // Callback for startup.
  void onStartup();

  // Callback for shutdown.
  void onShutdown(){};

private:
  const char* viewNames[4] = {"Main", "Side", "Front", "Top"};
  std::vector<int> layoutIdx = {14, 13, 5}; // location of the views in the layout
  std::vector<int> viewDirections = {1, 2, -3};
  std::vector<vtkSmartPointer<vtkSMProxyLink>> linkStorage;
  std::vector<vtkSmartPointer<vtkSMViewLink>> viewLinkStorage;

  Q_DISABLE_COPY(pqOrthographicViewStarter)

private Q_SLOTS:
  /**
   * @brief pqOrthographicViewStarter::setActiveViews
   * Refresh all views
  */
  void setActiveViews();

  /**
   * @brief pqOrthographicViewStarter::createReciprocalViewLink
   * Create link between 2 views without the camera links
  */
  void createReciprocalViewLink(vtkSMProxy* view1Proxy, vtkSMProxy* view2Proxy);

  /**
   * @brief pqOrthographicViewStarter::onRepresentationAdded
   * Called when a new representation is added to the pipeline
   * Create a data representation for each view and link them to the representation added
  */
  void onRepresentationAdded(pqRepresentation* rep);

  /**
   * @brief pqOrthographicViewStarter::onSourceAdded
   * Called when a new source is added to the pipeline
   * Set the 3D main view as the active view in order to create the data representations on the other views
  */
  void onSourceAdded(pqPipelineSource* source);

  /**
   * @brief pqOrthographicViewStarter::createDataRepresentation
   * Create the 3 data representations from the source and assign them to the 3 orthographic views
  */
  void createDataRepresentation(pqDataRepresentation* rep, pqPipelineSource* source);

  /**
   * @brief pqOrthographicViewStarter::createScalarBarRepresentation
   * Set oldest scalar bar representation with visibility off
  */
  void onColorTransferFunctionModified();
  
};

#endif
