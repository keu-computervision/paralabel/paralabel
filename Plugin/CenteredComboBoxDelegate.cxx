#include "CenteredComboBoxDelegate.h"
#include <QPainter>

void CenteredComboBoxDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    QStyleOptionViewItem centeredOption(option);
    centeredOption.displayAlignment = Qt::AlignCenter;
    QStyledItemDelegate::paint(painter, centeredOption, index);
}
