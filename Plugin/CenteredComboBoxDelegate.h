// SPDX-FileCopyrightText: Copyright (c) Kitware Inc.
// SPDX-FileCopyrightText: Copyright (c) Sandia Corporation
// SPDX-License-Identifier: BSD-3-Clause
/**
 * @class CenteredComboBoxDelegate
 * @brief This class is used to center the text in a combobox
*/

#ifndef centeredcomboboxdelegate_h
#define centeredcomboboxdelegate_h

#include <QStyledItemDelegate>

class CenteredComboBoxDelegate : public QStyledItemDelegate
{
    Q_OBJECT

public:
    explicit CenteredComboBoxDelegate(QObject* parent = nullptr) : QStyledItemDelegate(parent) {}

    void paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const override;
};

#endif
